package br.com.alienExplodes.servlets;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@WebServlet("/DeslogarUsuario")
public class DeslogarUsuario extends HttpServlet{

	public DeslogarUsuario()throws Exception{}
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1505599867024332325L;
	
	@SuppressWarnings("unchecked")
	protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		String usuario = request.getParameter("usuario").toLowerCase();
		
		if (usuario != null) {
			//Cria o parse de tratamento
		    JSONParser parser = new JSONParser();
			
	        String baseLogados = this.getServletContext().getRealPath("arquivosPersistencia/usuariosLogados.json");
	        
			JSONObject jsonLogados;	        		        	    
	        jsonLogados = (JSONObject) parser.parse(new FileReader(baseLogados));
	        List<JSONObject> listLogados = (ArrayList<JSONObject>)jsonLogados.get("usuariosLogados");
	        
	        for (JSONObject objIteracao : listLogados) {
	        	if (objIteracao.get("nomeUsuario") != null && objIteracao.get("nomeUsuario").equals(usuario)) {
	        		listLogados.remove(objIteracao);
	        		break;
				} 
			}
	        
	        JSONObject objPersistir = new JSONObject();
	        objPersistir.put("usuariosLogados", listLogados);
	        
	        FileWriter writeFile = new FileWriter(baseLogados,false);
	        writeFile.write(objPersistir.toJSONString());
	        writeFile.close();
	        //-------------------------------------------------------//
	        resposta("logado", response);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			this.processRequest(req, resp);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
	}
	
	protected HttpServletResponse resposta(String mensagem, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mensagem);
		return response;
	}
	
}



package br.com.alienExplodes.servlets;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


@WebServlet("/CadastrarUsuarios")
public class CadastrarUsuarios extends HttpServlet {

	public CadastrarUsuarios()throws Exception{}
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1505599867024332325L;
	
	@SuppressWarnings("unchecked")
	protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		String novoUsuario = request.getParameter("novoUsuario").toLowerCase();
		String novaSenha = request.getParameter("novaSenha");
		String confirmacaoSenha = request.getParameter("confirmacaoSenha");
		String emailUsuario = request.getParameter("emailUsuario").toLowerCase();
		String nomeRegex = "^\\w+$";
		String emailRegex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		
		String filePath = this.getServletContext().getRealPath("arquivosPersistencia/usuarios.json"); 
		
		//Referencia de objeto JSON
		JSONObject jsonObject;
		
		//Cria o parse de tratamento
	    JSONParser parser = new JSONParser();
	    
	    //Salva no objeto JSONObject o que o parse tratou do arquivo
        jsonObject = (JSONObject) parser.parse(new FileReader(filePath));
        
        if (novoUsuario.equals("") || novoUsuario.length() < 3 || novoUsuario.length() > 12) {
        	response = resposta("{\"param1\":\"nomeCadastro\", \"param2\":\"Favor inserir o nome de 3 a 12 caracteres.\"}", response);
		}
        else if (novoUsuario.matches(nomeRegex) == false) {
        	response = resposta("{\"param1\":\"nomeCadastro\", \"param2\":\"Nome inv�lido.\"}", response);
        }
        else if(novaSenha.equals("")) {
        	response = resposta("{\"param1\":\"senhaCadastro\", \"param2\":\"Favor inserir a senha.\"}", response);
        }
		else if(confirmacaoSenha.equals("")) {
			response = resposta("{\"param1\":\"confSenhaCadastro\", \"param2\":\"Favor inserir a senha novamente.\"}", response);
		}
		else if(emailUsuario.equals("")) {
			response = resposta("{\"param1\":\"emailCadastro\", \"param2\":\"Favor inserir o email.\"}", response);
		}
		else if(emailUsuario.matches(emailRegex) == false) {
			response = resposta("{\"param1\":\"emailCadastro\", \"param2\":\"Favor inserir o email.\"}", response);
		}
		else {
			if (!jsonObject.containsKey(novoUsuario)) {	
	        	if (novaSenha.equals(confirmacaoSenha)) {
	            	JSONObject dadosUsuario = new JSONObject();
	            	dadosUsuario.put("senha", novaSenha);
	    			dadosUsuario.put("email", emailUsuario);
	            	
	    			jsonObject.put(novoUsuario, dadosUsuario);
	            	
	    			//Escreve no arquivo conteudo do Objeto JSON
	            	FileWriter writeFile = new FileWriter(filePath);
	                writeFile.write(jsonObject.toJSONString());
	                writeFile.close();
	                
	                response = resposta("sucesso", response);
				}
	        	else {
					response = resposta("{\"param1\":\"confSenhaCadastro\", \"param2\":\"Senhas n�o correspondem.\"}", response);
				}
			}
	        else {
	        	response = resposta("{\"param1\":\"nomeCadastro\", \"param2\":\"O usu�rio j� existe.\"}", response);
	        }
		}
	}    
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			this.processRequest(req, resp);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
	}
	
	protected HttpServletResponse resposta(String mensagem, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mensagem);
		return response;
	}
	
}



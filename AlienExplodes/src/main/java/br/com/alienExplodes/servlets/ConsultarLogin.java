package br.com.alienExplodes.servlets;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@WebServlet("/ConsultarLogin")
public class ConsultarLogin extends HttpServlet{

	public ConsultarLogin()throws Exception{}
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1505599867024332325L;
	
	@SuppressWarnings("unchecked")
	protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		String usuario = request.getParameter("usuario").toLowerCase();
		
		if (usuario == null) {
			
		}
		else {
			//Cria o parse de tratamento
		    JSONParser parser = new JSONParser();
			
	        String baseLogados = this.getServletContext().getRealPath("arquivosPersistencia/usuariosLogados.json");
	        
			JSONObject jsonLogados;	        		        	    
	        jsonLogados = (JSONObject) parser.parse(new FileReader(baseLogados));
	        List<JSONObject> listLogados = (ArrayList<JSONObject>)jsonLogados.get("usuariosLogados");
	        
	        for (JSONObject objIteracao : listLogados) {
	        	if (objIteracao.get("nomeUsuario") != null && objIteracao.get("nomeUsuario").equals(usuario)) {
	        		resposta("logado", response);
				} 
			}
	        //-------------------------------------------------------//
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			this.processRequest(req, resp);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
	}
	
	protected HttpServletResponse resposta(String mensagem, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mensagem);
		return response;
	}
	
}



package br.com.alienExplodes.servlets;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class ConsultarRanking extends HttpServlet{

	public ConsultarRanking()throws Exception{}
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1505599867024332325L;
	
	@SuppressWarnings("unchecked")
	protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String usuario = request.getParameter("usuario");
		
		String filePath = this.getServletContext().getRealPath("arquivosPersistencia/ranking.json"); 
		
		//Referencia de objeto JSON
		JSONObject jsonObject;
		
		//Cria o parse de tratamento
	    JSONParser parser = new JSONParser();
	    
	    //Salva no objeto JSONObject o que o parse tratou do arquivo
        jsonObject = (JSONObject) parser.parse(new FileReader(filePath));
        
		List<JSONObject> listObjs =  (ArrayList<JSONObject>) jsonObject.get("ranking");
		
		listObjs = (List<JSONObject>) ordenaRanking(listObjs);
		
		List<JSONObject> objsResponse =  new ArrayList<JSONObject>();
		
		//Popula objsResponse com x primeiros valores do ranking ordenado
		int contador = 0;
		for (JSONObject objIteracao : listObjs) {
			contador++;
			if (objIteracao != null) {
				objsResponse.add(objIteracao);
			}
			if(contador == 5) {
				break;
			}
		}
		
		JSONObject rankingUsuario = null;
		
		//Gera linha do usuario logado
		if (usuario != "convidado") {
			int contadorIndexUsu = 0;
			for (JSONObject objIteracao : listObjs) {
				if (objIteracao.containsValue(usuario)) {
					objIteracao.put("posicao", contadorIndexUsu);
					rankingUsuario = objIteracao;
					break;
				}
				contadorIndexUsu++;
			}
		}
		
		//Caso o usuario n�o esteja no ranking ou seja convidado
		if (rankingUsuario == null) {
			rankingUsuario = new JSONObject();
			rankingUsuario.put("posicao","?");
			rankingUsuario.put("nome","???");
			rankingUsuario.put("pontuacao","??");
		}
		
		//Gera obj que vai ser passado para o JS
		response = resposta("{\"ranking\":"+objsResponse.toString()+", \"usuarioPontuacao\":"+rankingUsuario.toString()+"}", response);
	}
	
	//Retorna o ranking ordenado
	private List<JSONObject> ordenaRanking(List<JSONObject> listaRegistros) {
		List<JSONObject> objetosOrdenados = new ArrayList<JSONObject>();
		
		int tamanhoOriginal = listaRegistros.size();
		
		while(listaRegistros.size() > 0) {
			long pontosObjI = (long) listaRegistros.get(0).get("pontuacao");
			long tempoObjI = (long) listaRegistros.get(0).get("tempoGasto");
			JSONObject maiorObjIteracao = null;
			
			for (int j = 0; j<listaRegistros.size(); j++) {
				long pontosObjJ = (long) listaRegistros.get(j).get("pontuacao");
				long tempoObjJ = (long) listaRegistros.get(j).get("tempoGasto");
				
				if (pontosObjI != pontosObjJ|| tempoObjI != tempoObjJ) {
					if (pontosObjJ > pontosObjI) {
						pontosObjI = pontosObjJ;
						maiorObjIteracao = listaRegistros.get(j);
					}
					else if(pontosObjJ == pontosObjI){
						if (tempoObjJ < tempoObjI) {
							pontosObjI = pontosObjJ;
							maiorObjIteracao = listaRegistros.get(j);
						}
					}
				}
				else {
					if (listaRegistros.size() == listaRegistros.size()) {
						maiorObjIteracao = listaRegistros.get(0);
					}
				}
			}
			if (maiorObjIteracao != null) {
				listaRegistros.remove(maiorObjIteracao);
				objetosOrdenados.add(maiorObjIteracao);
			}else {
				listaRegistros.remove(0);
			}
			
		}
		return (List<JSONObject>) objetosOrdenados;
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			this.processRequest(req, resp);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
	}
	
	protected HttpServletResponse resposta(String mensagem, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mensagem);
		return response;
	}
	
}



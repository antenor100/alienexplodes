package br.com.alienExplodes.servlets;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@WebServlet("/ConsultarUsuarios")
public class ConsultarUsuarios extends HttpServlet{

	public ConsultarUsuarios()throws Exception{}
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1505599867024332325L;
	
	@SuppressWarnings("unchecked")
	protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		String usuario = request.getParameter("usuario").toLowerCase();
		String senha = request.getParameter("senha");
		String nomeRegex = "^\\w+$";
		
		String filePath = this.getServletContext().getRealPath("arquivosPersistencia/usuarios.json"); 
		
		//Referencia de objeto JSON
		JSONObject jsonObject;
		
		//Cria o parse de tratamento
	    JSONParser parser = new JSONParser();
	    
	    //Salva no objeto JSONObject o que o parse tratou do arquivo
        jsonObject = (JSONObject) parser.parse(new FileReader(filePath));
        
        if (usuario.equals("") || usuario.length() < 3 || usuario.length() > 12) {
        	response = resposta("{\"param1\":\"nomeEntrar\", \"param2\":\"Favor inserir o nome.\"}", response);
		}
        else if (usuario.matches(nomeRegex) == false) {
        	response = resposta("{\"param1\":\"nomeCadastro\", \"param2\":\"Nome inv�lido.\"}", response);
        }
        else if(senha.equals("")) {
        	response = resposta("{\"param1\":\"senhaEntrar\", \"param2\":\"Favor inserir a senha.\"}", response);
		}
		else {
			if (jsonObject.containsKey(usuario)) {
				JSONObject dadosUsuario = (JSONObject) jsonObject.get(usuario);
	        	if (dadosUsuario.get("senha").equals(senha)) {
	        		response = resposta("sucesso",response);
	        		
	        		//seta usuario como logado
	                String baseLogados = this.getServletContext().getRealPath("arquivosPersistencia/usuariosLogados.json"); 	            	               
	        		JSONObject jsonLogados;	        		        	    
	                jsonLogados = (JSONObject) parser.parse(new FileReader(baseLogados));
	                List<JSONObject> listLogados = (ArrayList<JSONObject>)jsonLogados.get("usuariosLogados");
	                
	                boolean salvaUsuarioLogado = true;
	                
	                for (JSONObject objIteracao : listLogados) {
	                	if (objIteracao.get("nomeUsuario") != null && objIteracao.get("nomeUsuario").equals(usuario)) {
							salvaUsuarioLogado = false;	
						} 
					}
	                
	                if (salvaUsuarioLogado) {
	                	JSONObject objUsuario = new JSONObject();
	                	objUsuario.put("nomeUsuario", usuario);
						listLogados.add(objUsuario);
					}
	                
	                JSONObject objPersistir = new JSONObject();
	                objPersistir.put("usuariosLogados", listLogados);
	                
	                FileWriter writeFile = new FileWriter(baseLogados,false);
	                writeFile.write(objPersistir.toJSONString());
	                writeFile.close();
	                //-------------------------------------------------------//
	        		
				}
	        	else {
					response = resposta("{\"param1\":\"senhaEntrar\", \"param2\":\"Senha incorreta.\"}", response);
				}
			}
	        else {
	        	response = resposta("{\"param1\":\"nomeEntrar\", \"param2\":\"Usu�rio n�o cadastrado.\"}", response);
	        }
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			this.processRequest(req, resp);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
	}
	
	protected HttpServletResponse resposta(String mensagem, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mensagem);
		return response;
	}
	
}



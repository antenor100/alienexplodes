package br.com.alienExplodes.servlets;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@WebServlet("/CadastraRanking")
public class CadastrarRanking extends HttpServlet{

	public CadastrarRanking()throws Exception{}
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1505599867024332325L;
	
	@SuppressWarnings("unchecked")
	protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		String usuario = request.getParameter("usuario").toLowerCase();
		long pontuacao = Long.parseLong(request.getParameter("pontuacao"));
		long tempoGasto = Long.parseLong(request.getParameter("tempoGasto"));
		
		String filePath = this.getServletContext().getRealPath("arquivosPersistencia/ranking.json"); 
		
		//Referencia de objeto JSON
		JSONObject jsonObject;
		
		//Cria o parse de tratamento
	    JSONParser parser = new JSONParser();
	    
	    //Salva no objeto JSONObject o que o parse tratou do arquivo
        jsonObject = (JSONObject) parser.parse(new FileReader(filePath));
        
		List<JSONObject> listObjs =  (ArrayList<JSONObject>) jsonObject.get("ranking");
		
		JSONObject objetoAhRemover = null;
		
		boolean contemRegistro = false;
		
		for (JSONObject objIteracao : listObjs) {
			if (objIteracao.containsValue(usuario)) {
				contemRegistro = true;
				if ((long)(objIteracao.get("pontuacao")) < pontuacao) {
					objetoAhRemover = objIteracao;
				}else {
					if ((long)(objIteracao.get("pontuacao")) == pontuacao && (long)(objIteracao.get("tempoGasto")) > tempoGasto) {
						objetoAhRemover = objIteracao;
					}
				}
			}
		}
		
		if (!(contemRegistro && objetoAhRemover == null)) {
			
			if (objetoAhRemover != null) {
				listObjs.remove(objetoAhRemover);
			}
			
			JSONObject objRequest = new JSONObject();
			objRequest.put("nome", usuario);
			objRequest.put("pontuacao", pontuacao);
			objRequest.put("tempoGasto", tempoGasto);
			
			listObjs.add(objRequest);
	        
	        JSONObject objPersistir = (JSONObject) new JSONObject();
	        objPersistir.put("ranking", listObjs);
	        
	        FileWriter writeFile = new FileWriter(filePath,false);
	        writeFile.write(objPersistir.toJSONString());
	        writeFile.close();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			this.processRequest(req, resp);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
	}
	
	protected HttpServletResponse resposta(String mensagem, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mensagem);
		return response;
	}
	
}



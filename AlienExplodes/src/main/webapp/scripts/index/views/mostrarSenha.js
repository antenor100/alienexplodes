function mostrarSenha(idSenha, idEye, idSpan) {
    let inputSenha = document.getElementById(idSenha);
    let inputEye = document.getElementById(idEye);
    let mostrarSenha = document.getElementById(idSpan);
    inputSenha.focus();
    if (inputSenha.type == "password") {
        inputEye.src = "imagens/index/eye-open.png";
        inputSenha.type = "text";
        mostrarSenha.innerHTML = "Ocultar Senha";
        setTimeout(function(){
            inputSenha.selectionStart = inputSenha.selectionEnd = 1000;
        }, 0);
    }
    else {
        inputEye.src = "imagens/index/eye-closed.png";
        inputSenha.type = "password";
        mostrarSenha.innerHTML = "Mostrar Senha";
        setTimeout(function(){
            inputSenha.selectionStart = inputSenha.selectionEnd = 1000;
        }, 0);
    }
}

function populaRankingTela(arrayElementos, objJogador) {
    
    let telaDivVazio = document.getElementById("registrosRanking");
    let arraySeExisteRegistros = document.getElementById("registrosRanking").children;

    if (arraySeExisteRegistros.length > 0) {
        Array.from(arraySeExisteRegistros).forEach(element => {
            element.remove();
        });
    }

    arrayElementos.forEach(element => {
        telaDivVazio.appendChild(element);
    }); 
   
    if (objJogador.posicao == "?") {
        document.getElementById("suaPosicao").innerHTML = (objJogador.posicao);
    }else{
        document.getElementById("suaPosicao").innerHTML = (objJogador.posicao + 1) + "º";
    }
    document.getElementById("seuNome").innerHTML = objJogador.nome;
    document.getElementById("suaPontuacao").innerHTML = objJogador.pontuacao;
   
}
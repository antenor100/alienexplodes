function mostrarAviso(objString) {
	
	let obj = JSON.parse(objString);
	
    let inputAviso = obj.param1;
    let mensagem = obj.param2;

    if (inputAviso == "nomeEntrar") {
        document.getElementById("nomeEntrar").style.border = "1px inset red";
        document.getElementById("nomeEntrar").focus();
        document.getElementById("tdNomeEntrar").style.padding = "0px";
        document.getElementById("pNomeEntrar").style.display = "block";
        document.getElementById("spanNomeEntrar").innerHTML = mensagem;

        ocultarAviso("senhaEntrar", "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
        ocultarAviso("nomeCadastro", "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
        ocultarAviso("senhaCadastro", "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
        ocultarAviso("confSenhaCadastro", "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
        ocultarAviso("emailCadastro", "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");
    }
    else if (inputAviso == "senhaEntrar") {
        document.getElementById("senhaEntrar").style.border = "1px inset red";
        document.getElementById("senhaEntrar").focus();
        document.getElementById("tdSenhaEntrar").style.padding = "0px";
        document.getElementById("pSenhaEntrar").style.display = "block";
        document.getElementById("spanSenhaEntrar").innerHTML = mensagem;

        ocultarAviso("nomeEntrar", "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
        ocultarAviso("nomeCadastro", "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
        ocultarAviso("senhaCadastro", "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
        ocultarAviso("confSenhaCadastro", "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
        ocultarAviso("emailCadastro", "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");
    }
    else if (inputAviso == "nomeCadastro") {
        document.getElementById("nomeCadastro").style.border = "1px inset red";
        document.getElementById("nomeCadastro").focus();
        document.getElementById("tdNomeCadastro").style.padding = "0px";
        document.getElementById("pNomeCadastro").style.display = "block";
        document.getElementById("spanNomeCadastro").innerHTML = mensagem;

        ocultarAviso("nomeEntrar", "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
        ocultarAviso("senhaEntrar", "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
        ocultarAviso("senhaCadastro", "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
        ocultarAviso("confSenhaCadastro", "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
        ocultarAviso("emailCadastro", "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");
    }
    else if (inputAviso == "senhaCadastro") {
        document.getElementById("senhaCadastro").style.border = "1px inset red";
        document.getElementById("senhaCadastro").focus();
        document.getElementById("tdSenhaCadastro").style.padding = "0px";
        document.getElementById("pSenhaCadastro").style.display = "block";
        document.getElementById("spanSenhaCadastro").innerHTML = mensagem;

        ocultarAviso("nomeEntrar", "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
        ocultarAviso("senhaEntrar", "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
        ocultarAviso("nomeCadastro", "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
        ocultarAviso("confSenhaCadastro", "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
        ocultarAviso("emailCadastro", "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");
    }
    else if (inputAviso == "confSenhaCadastro") {
        document.getElementById("confSenhaCadastro").style.border = "1px inset red";
        document.getElementById("confSenhaCadastro").focus();
        document.getElementById("tdConfSenhaCadastro").style.padding = "0px";
        document.getElementById("pConfSenhaCadastro").style.display = "block";
        document.getElementById("spanConfSenhaCadastro").innerHTML = mensagem;

        ocultarAviso("nomeEntrar", "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
        ocultarAviso("senhaEntrar", "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
        ocultarAviso("nomeCadastro", "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
        ocultarAviso("senhaCadastro", "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
        ocultarAviso("emailCadastro", "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");
    }
    else if (inputAviso == "emailCadastro") {
        document.getElementById("emailCadastro").style.border = "1px inset red";
        document.getElementById("emailCadastro").focus();
        document.getElementById("tdEmailCadastro").style.padding = "0px";
        document.getElementById("pEmailCadastro").style.display = "block";
        document.getElementById("spanEmailCadastro").innerHTML = mensagem;

        ocultarAviso("nomeEntrar", "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
        ocultarAviso("senhaEntrar", "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
        ocultarAviso("nomeCadastro", "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
        ocultarAviso("senhaCadastro", "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
        ocultarAviso("confSenhaCadastro", "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
    }
}

function ocultarTodosAvisos() {
    ocultarAviso("nomeEntrar", "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
    ocultarAviso("senhaEntrar", "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
    ocultarAviso("nomeCadastro", "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
    ocultarAviso("senhaCadastro", "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
    ocultarAviso("confSenhaCadastro", "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
    ocultarAviso("emailCadastro", "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");
}

function ocultarAviso(inputAviso, idInput, idPAviso, idSpanAviso) {
    document.getElementById(inputAviso).style.border = "1px inset lightgray";
    document.getElementById(idInput).style.padding = "5px";
    document.getElementById(idPAviso).style.display = "none";
    document.getElementById(idSpanAviso).innerHTML = "";
}
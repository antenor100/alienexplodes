function mostrarCadastro(divChange) {
    let divEntrar = document.getElementById("divEntrar");
    let divCadastrar = document.getElementById("divCadastrar");
    
    if (divChange == "divEntrar") {
        divEntrar.style.visibility = "hidden";
        divEntrar.style.opacity = "0";
        setTimeout(() => {
            divCadastrar.style.visibility = "visible";
            divCadastrar.style.opacity = "1";
        }, 500);
    }
    else if (divChange == "divCadastrar") {
        divCadastrar.style.visibility = "hidden";
        divCadastrar.style.opacity = "0";
        setTimeout(() => {
            divEntrar.style.visibility = "visible";
            divEntrar.style.opacity = "1";
        }, 500);
    }
}

function ocultarCadastro() {
	
	ocultarTodosAvisos();
    let divCadastroRealizado = document.getElementById("divCadastroRealizado");
    
	divCadastrar.style.visibility = "hidden";
    divCadastrar.style.opacity = "0";
    setTimeout(() => {
    	divCadastroRealizado.style.visibility = "visible";
    	divCadastroRealizado.style.opacity = "1";
    }, 500);
}

function mostrarLogin() {
    let divCadastroRealizado = document.getElementById("divCadastroRealizado");
    
	divCadastroRealizado.style.visibility = "hidden";
	divCadastroRealizado.style.opacity = "0";
    setTimeout(() => {
        divEntrar.style.visibility = "visible";
        divEntrar.style.opacity = "1";
    }, 500);
}

function mostrarBoasVindas() {
    ocultarTodosAvisos();
    document.getElementById("fecharDiv").setAttribute("onclick", "logout(), buttonPress();");
    
    let divEntrar = document.getElementById("divLogin");
    let divBoasVindas = document.getElementById("divLogado");
    divEntrar.style.opacity = "0";
    divBoasVindas.style.display = "flex";
    centralizaImagemDiv("divLogado", divBoasVindas.clientWidth, divBoasVindas.clientHeight);
}

function logout() {
	document.getElementById("fecharDiv").setAttribute("onclick", "fechaLogin('divLogin', 'logo', 'imgLogo', 'aviso'), buttonPress();");
	setTimeout(() => {
		document.getElementById("divLogin").style.width = "350px";
		document.getElementById("divLogin").style.marginLeft = "-175px";
	}, 500);
	mostrarLogin();
}
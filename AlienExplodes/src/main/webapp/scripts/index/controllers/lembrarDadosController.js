function localSetDadosSalvos(usuario, senha){

    let statusCheck = document.getElementById("lembrarDados").checked;

    if (statusCheck) {
        setLocaleData("dadosSalvos", {usuario:usuario,senha:senha,statusCheck:statusCheck});
    }else{
        removeLocaleData("dadosSalvos");
    }
}

function localGetDadosSalvos(){

    if (getLocaleData("dadosSalvos") != null) {
        
        let dadosSalvos = getLocaleData("dadosSalvos");

        document.getElementById("nomeEntrar").value = dadosSalvos.usuario;
        document.getElementById("senhaEntrar").value = dadosSalvos.senha;
        document.getElementById("lembrarDados").checked = dadosSalvos.statusCheck;
    }
}
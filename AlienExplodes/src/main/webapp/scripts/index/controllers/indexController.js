function fundoResponsivo(stringComSeparador){
    
    let corpoInteiro = document.getElementById("corpoInteiro");
    let widthCorpointeiro = parseInt(corpoInteiro.style.width.substring(0, corpoInteiro.style.width.length-2), 10);
    let bodyWidth = document.body.clientWidth;
    let logo = document.getElementById("logo");
    let divVolumeControl = document.getElementById("divVolumeControl");
    
    if (widthCorpointeiro < bodyWidth) {
        logo.style.position = "absolute";
        divVolumeControl.style.position = "absolute";
    }
    else {
        logo.style.position = "fixed";
        divVolumeControl.style.position = "fixed";
    }
    
    let arrayIdFundo = stringComSeparador.split("/");

    for (let i = 0; i < arrayIdFundo.length; i++) {
        let img = document.getElementById(arrayIdFundo[i]);
        img.style.width = (window.innerHeight * 1.777777777) + "px";
        img.style.height = window.innerHeight + "px";
        img.style.left = "50%"; 
        img.style.marginLeft = `${-(window.innerHeight * 1.7777777777) / 2}px`
    }

}

function centralizaImagemDiv(idElement, width, heigth=width){
    let elemento = document.getElementById(idElement);
    elemento.style.top = "50%"; 
    elemento.style.left = "50%"; 
    elemento.style.marginLeft = `${-(width) / 2}px`;
    elemento.style.marginTop = `${-(heigth) / 2}px`;
}

function mostraLogin(idDivLogin, idDivLogo, idImgLogo, idAviso) {
    let divLogin = document.getElementById(idDivLogin);
    let divLogo = document.getElementById(idDivLogo);
    let imgLogo = document.getElementById(idImgLogo);
    let aviso = document.getElementById(idAviso);

    aviso.style.display = "none";

    divLogin.style.display = "block";
    divLogin.style.marginTop = "-205px";
    divLogin.style.opacity = "1";
    imgLogo.style.width = "220px";
    imgLogo.style.height = "220px";
    imgLogo.style.cursor = "default";
    imgLogo.setAttribute('onclick', '');
    divLogo.style.margin = "0px";
    divLogo.style.left = "10px";
    divLogo.style.top = "10px";

    setTimeout(function () {
        divLogin.style.top = "50%"; 
    }, 50);
}

function ocultarLogo(idDivLogo, idImgLogo, idAviso) {
    let divLogo = document.getElementById(idDivLogo);
    let imgLogo = document.getElementById(idImgLogo);
    let aviso = document.getElementById(idAviso);

    aviso.style.display = "none";

    divLogin.style.display = "block";
    divLogin.style.marginTop = "-205px";
    divLogin.style.opacity = "1";
    imgLogo.style.width = "220px";
    imgLogo.style.height = "220px";
    imgLogo.style.cursor = "default";
    imgLogo.setAttribute('onclick', '');
    divLogo.style.margin = "0px";
    divLogo.style.left = "10px";
    divLogo.style.top = "10px";
}

function fechaLogin(idDivLogin, idDivLogo, idImgLogo, idAviso) {
    let divLogin = document.getElementById(idDivLogin);
    let imgLogo = document.getElementById(idImgLogo);
    let aviso = document.getElementById(idAviso);

    imgLogo.style.width = "500px";
    imgLogo.style.height = "500px";
    imgLogo.style.cursor = "pointer";
    imgLogo.setAttribute('onclick', "mostraLogin('divLogin', 'logo', 'imgLogo', 'aviso'), buttonPress();");

    centralizaImagemDiv(idDivLogo,500,530);

    setTimeout(function () {
        divLogin.style.top = "150%";
        divLogin.style.marginTop = "0px";
        divLogin.style.opacity = "0";
        aviso.style.display = "inline-block";   
    }, 1);    
}

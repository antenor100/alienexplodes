function populaRanking(obj) {
    let arrayObjsRanking = obj.ranking;
    let posicaoJogador = obj.usuarioPontuacao;
    
    let arrayRegistros = [];
    
    arrayObjsRanking.forEach(element => {      
        let divRegistro = document.createElement("div");
        divRegistro.classList.add("itemRanking");
        
        let divPosicao = document.createElement("div");
        divPosicao.classList.add("posicaoW");
        
        if((arrayObjsRanking.indexOf(element)+1) <= 3){
            let imgPosicao = document.createElement("img");          
            imgPosicao.setAttribute("src",`imagens/index/ranking${(arrayObjsRanking.indexOf(element)+1)}.png`);
            divPosicao.appendChild(imgPosicao);

        }else{
            divPosicao.innerHTML = (arrayObjsRanking.indexOf(element)+1) +"º";
        }

        let divNome = document.createElement("div");
        divNome.classList.add("nomeW");
        divNome.innerHTML = element.nome;

        let divPontuacao = document.createElement("div");
        divPontuacao.classList.add("pontuacaoW");
        divPontuacao.innerHTML = element.pontuacao;

        divRegistro.appendChild(divPosicao);
        divRegistro.appendChild(divNome);
        divRegistro.appendChild(divPontuacao);

        arrayRegistros.push(divRegistro);
    });

    populaRankingTela(arrayRegistros, posicaoJogador)
}
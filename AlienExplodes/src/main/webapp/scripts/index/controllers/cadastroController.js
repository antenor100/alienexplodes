function setUsuarios(inputUsu, inputSenha, inputConfSenha, inputEmail) {

    inputUsuValue = document.getElementById(inputUsu).value;
    inputSenhaValue = document.getElementById(inputSenha).value;
    inputConfSenhaValue = document.getElementById(inputConfSenha).value;
    inputEmailValue = document.getElementById(inputEmail).value;

    let regexNome = "[^A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ 0-9]+";
    let regexEmail = "^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";

    if (inputUsuValue != "") {
        
        if (!inputUsuValue.match(regexNome)) {

            ocultarAviso(inputUsu, "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
            ocultarAviso(inputSenha, "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
            ocultarAviso(inputConfSenha, "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
            ocultarAviso(inputEmail, "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro");

            if (inputSenhaValue != "") {
    
                if (inputConfSenhaValue != "") {
    
                    if (inputEmailValue != "") {
    
                        if (inputEmailValue.match(regexEmail)) {

                            let bancoUsuarios = [];
                            let repositorio = getUsuarios();
                            
                            if (repositorio != null) {
                                bancoUsuarios = getUsuarios();
                            }
                            else{
                                repositorio = null;
                            }
        
                            let linha = {usuario:"",senha:"",email:""};
        
                            linha.usuario = inputUsuValue;
                            linha.senha = inputSenhaValue;
                            linha.email = inputEmailValue;
                            bancoUsuarios.push(linha);
        
                            if (linha.senha != inputConfSenhaValue) {
                                
                                mostrarAviso(inputConfSenha, "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro", "Senhas não coincidem.");
                                document.getElementById(inputSenha).style.border = "1px inset red";
        
                            }
                            else{

                                ocultarAviso(inputConfSenha, "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro");
                                document.getElementById(inputSenha).style.border = "1px inset lightgray";
        
                                if (repositorio == null) {
                                    
                                    setLocaleData("usuarios",bancoUsuarios)
                                    alert("Cadastro realizado com sucesso.");
        
                                }
                                else{
        
                                    let possuiUsuario = false;
        
                                    repositorio.forEach(i => {
                                        if (i.usuario.toLowerCase() == linha.usuario.toLowerCase()) {
                                            possuiUsuario = true;
                                        }
                                    });
        
                                    if (possuiUsuario) {
                                        
                                        mostrarAviso(inputUsu, "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro", "Usuário já existe.");
                                        
                                    }
                                    else{
                                        ocultarAviso(inputUsu, "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro");
                                        
                                        removeLocaleData("usuarios")
                                        setLocaleData("usuarios",bancoUsuarios)
                                        alert("Cadastro realizado com sucesso.");
                                    }
        
                                }
                            }
                        }
                        else {
                            mostrarAviso(inputEmail, "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro", "Favor inserir um e-mail válido.");
                        }
                    }
                    else {
                        mostrarAviso(inputEmail, "tdEmailCadastro", "pEmailCadastro", "spanEmailCadastro", "Favor inserir um e-mail.");
                    }
                }
                else {
                    mostrarAviso(inputConfSenha, "tdConfSenhaCadastro", "pConfSenhaCadastro", "spanConfSenhaCadastro", "Favor inserir a senha novamente.");
                }
            }
            else {
                mostrarAviso(inputSenha, "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro", "Favor inserir uma senha.");
            }
        }
        else {
            mostrarAviso(inputUsu, "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro", "Não é permitido o uso de caracteres especiais.");
        }
    }
    else {
        mostrarAviso(inputUsu, "tdNomeCadastro", "pNomeCadastro", "spanNomeCadastro", "Favor inserir um nome.");
        ocultarAviso(inputSenha, "tdSenhaCadastro", "pSenhaCadastro", "spanSenhaCadastro");
    }
}

function getUsuarios() {     
        
    let bancoUsuarios = getLocaleData("usuarios");  
    return bancoUsuarios;
}
function validarLogin(inputUsu, inputSenha) {

    let regexNome = "[^A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ 0-9]+";
    let nome = document.getElementById("nomeEntrar").value.toLowerCase();
    let senha = document.getElementById("senhaEntrar").value;
    let usuarios = getLocaleData("usuarios");;
    let contemUsuario = false;
    let i;

    if (nome != "") {
        
        if (!nome.match(regexNome)) {

            ocultarAviso(inputUsu, "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar");
            ocultarAviso(inputSenha, "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
            
            if(senha != "") {

                if (getLocaleData("usuarios") != null) {
                    for (i = 0; i < usuarios.length; i++) {

                        if (usuarios[i].usuario.toLowerCase() == nome) {
                            
                            contemUsuario = true;
                            break;
                        }
                    }
    
                    if (contemUsuario) {
    
                        if (usuarios[i].senha == senha) {

                            localSetDadosSalvos(nome, senha);
                            setInterval(() => {window.location.href='game.html'}, 100);
                        }
                        else {
                            mostrarAviso(inputSenha, "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar", "Senha incorreta.");
                        }
                    }
                    else {
                        mostrarAviso(inputUsu, "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar", "Usuário não cadastrado.");
                    }
                }
                else {
                    mostrarAviso(inputUsu, "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar", "Usuário não cadastrado.");
                }
            }
            else {
                mostrarAviso(inputSenha, "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar", "Favor inserir uma senha.");
            }
        }
        else {
            mostrarAviso(inputUsu, "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar", "Não é permitido o uso de caracteres especiais.");
        }
    }
    else {
        mostrarAviso(inputUsu, "tdNomeEntrar", "pNomeEntrar", "spanNomeEntrar", "Favor inserir nome de usuário.");
        ocultarAviso(inputSenha, "tdSenhaEntrar", "pSenhaEntrar", "spanSenhaEntrar");
    }
}
function getLocaleData(dataName) {
    return JSON.parse(localStorage.getItem(dataName));
}

function setLocaleData(dataName, json) {
    let stringJson = JSON.stringify(json);
    localStorage.setItem(dataName, stringJson);
}

function setLocaleDataString(dataName, text) {
    localStorage.setItem(dataName, text);
}

function removeLocaleData(dataName) {
    localStorage.removeItem(dataName);
}

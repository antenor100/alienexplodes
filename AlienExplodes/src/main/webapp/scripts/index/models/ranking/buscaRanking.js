function rankingConsulta() {
    //novo httprequest
    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição: Post e a URL da API
    ajax.open("POST", "consultaRanking", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Seta paramêtros da requisição e envia a requisição
    let usuario = localStorage.getItem("usuarioLogado");

    ajax.send(`usuario=${usuario}`);

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function() {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
        	// Retorno do Ajax
        	var data = ajax.responseText;
            populaRanking(JSON.parse(data));
        }
    }
}

// Exemplo de requisição POST
function doPostServletCadastro() {
    //novo httprequest
    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição: Post e a URL da API
    ajax.open("POST", "cadastroUsuario", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Seta paramêtros da requisição e envia a requisição
    let usuario = document.getElementById("nomeCadastro");
    let senha = document.getElementById("senhaCadastro");
    let confirmacaoSenha = document.getElementById("confSenhaCadastro");
    let email = document.getElementById("emailCadastro");

    ajax.send(`${usuario.name}=${usuario.value}&${senha.name}=${senha.value}&${confirmacaoSenha.name}=${confirmacaoSenha.value}&${email.name}=${email.value}`);

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function() {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
            // Retorno do Ajax
            var data = ajax.responseText;
            if (data == "sucesso") {
            	ocultarCadastro();
            }
            else {
            	mostrarAviso(data);
            }
        }
    }
}

function doPostServletConsulta() {
    //novo httprequest
    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição: Post e a URL da API
    ajax.open("POST", "consultaUsuario", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Seta paramêtros da requisição e envia a requisição
    let usuario = document.getElementById("nomeEntrar");
    let senha = document.getElementById("senhaEntrar");

    ajax.send(`${usuario.name}=${usuario.value}&${senha.name}=${senha.value}`);

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function() {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
        	// Retorno do Ajax
        	var data = ajax.responseText;
            if (data == "sucesso") {
                mostrarBoasVindas();
                localStorage.setItem("usuarioLogado",usuario.value);
                rankingConsulta();
            }
            else {
            	mostrarAviso(data);
            }
        }
    }
}

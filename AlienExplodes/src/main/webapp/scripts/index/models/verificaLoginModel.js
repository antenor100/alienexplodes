function deslogarUsuario() {
    //novo httprequest
    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição: Post e a URL da API
    ajax.open("POST", "deslogaUsuario", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    ajax.send(`usuario=${localStorage.getItem("usuarioLogado")}`);

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function() {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
            // Retorno do Ajax
            var data = ajax.responseText;
            if (data == "logado") {
                localStorage.removeItem("usuarioLogado");
            }
            document.getElementById("divLogado").style.top = "-200%";
        }
    }
    fechaLogin('divLogin', 'logo', 'imgLogo', 'aviso');
}

function consultarLogin() {
    //novo httprequest
    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição: Post e a URL da API
    ajax.open("POST", "consultaLogin", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    ajax.send(`usuario=${localStorage.getItem("usuarioLogado")}`);

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function() {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
        	// Retorno do Ajax
        	var data = ajax.responseText;
            if (data == "logado") {
                setTimeout(() => {
                    mostrarBoasVindas();
                    ocultarLogo('logo', 'imgLogo', 'aviso');
                }, 100);
            }else{
                if (localStorage.getItem("usuarioLogado") != null) {
                    localStorage.removeItem("usuarioLogado");
                }
            }
        }
    }
}

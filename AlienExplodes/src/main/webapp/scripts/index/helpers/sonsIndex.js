function teclar() {
    let click = new Audio("sons/click.ogg");
    click.volume = 0.5;
    click.play();
}

function buttonPress() {
    let buttonPress = new Audio("sons/buttonPress.ogg");
    buttonPress.volume = 0.5;
    buttonPress.play();
}
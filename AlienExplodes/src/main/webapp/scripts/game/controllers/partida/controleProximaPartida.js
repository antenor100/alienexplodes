function proximaPartida() {
    mutarSomFundo();
    somStageClear();
    clearInterval(timerCowntdown);
    clearInterval(movimentacao);
    clearInterval(movimentarDownGrade);
    mostrarPontuacao();
    salvaVidasAoGanhar();
    salvaPontoAoGanhar();
    salvaTempoAoGanhar();
    salvaPowerAoGanhar();
    salvaFaseAoGanhar();
    setTimeout(() => {    	
    	alteraSpritePersonagemParado('personagem');
    }, 100);
    setTimeout(() => {
        window.location.reload();
    }, 3000);
}

function proximaPartidaBugada() {
    salvaVidasAoGanhar();
    salvaPontoAoGanhar();
    salvaTempoAoGanhar();
    salvaPowerAoGanhar();
    salvaFaseAoPerder();
    window.location.reload();
}
var timerPartida = () => timerCowntdown = setInterval(() => {

    tempoPartida = tempoPartida - 1000;

    if (tempoPartida >= 0) {
        let minutos = Math.floor((tempoPartida % (1000 * 60 * 60)) / (1000 * 60));
        let segundos = Math.floor((tempoPartida % (1000 * 60)) / 1000);
        if (segundos == 0) {
            document.getElementById("spanTempo").innerHTML = `${minutos}:${segundos}0`;
        } else if (segundos < 10) {
            document.getElementById("spanTempo").innerHTML = `${minutos}:0${segundos}`;
        }else {
            document.getElementById("spanTempo").innerHTML = `${minutos}:${segundos}`;
        }
    
    } else {
        finalizaPartida('tempo');
    }

}, 1000);
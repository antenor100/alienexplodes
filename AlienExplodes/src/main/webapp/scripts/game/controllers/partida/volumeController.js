function ajustarVolume(value) {

    let imgVolume = document.getElementById("imgVolumeControl");
    let somFundo = document.getElementById("somFundo");
    somFundo.volume = (value)/10;
    volume = (value)/10;

    if (value == 0 ) {
        imgVolume.src = "imagens/index/volumeMute.png";
    }
    else if (value <= 5) {
        imgVolume.src = "imagens/index/volumeLow.png";
    }
    else {
        imgVolume.src = "imagens/index/volumeHigh.png";
    }
}

function mutarSomFundo() {

    let volumeInput = document.getElementById("inputVolumeControl");
    let imgVolume = document.getElementById("imgVolumeControl");
    let somFundo = document.getElementById("somFundo");

    if (somFundo.volume != 0) {
        somFundo.volume = 0;
        volumeInput.value = "0";
        imgVolume.src = "imagens/index/volumeMute.png";
    }
    else {
        somFundo.volume = volume;
        somFundo.play();
        volumeInput.value = volume*10;
        imgVolume.src = "imagens/index/volumeLow.png";
    }
}

function mouseEnterVolumeControl() {

    let divVolumeControl = document.getElementById("divVolumeControl");
    let volumeInput = document.getElementById("inputVolumeControl");

    divVolumeControl.style.width = "150px";
    divVolumeControl.style.opacity = "1";
    divVolumeControl.style.boxShadow= "0px 0px 5px 1px #EA2400";

    setTimeout(() => {
        volumeInput.style.visibility = "visible";
    }, 250);
    
}

function mouseLeaveVolumeControl() {

    let divVolumeControl = document.getElementById("divVolumeControl");
    let volumeInput = document.getElementById("inputVolumeControl");

    divVolumeControl.style.width = "34px";
    divVolumeControl.style.opacity = "0.5";
    divVolumeControl.style.boxShadow= "0px 0px 1px 1px white";
    volumeInput.style.visibility = "hidden";

    setTimeout(() => {
        volumeInput.style.visibility = "hidden";
    }, 250);
    
}
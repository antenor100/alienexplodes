function finalizaPartida(tipoFim) {
    
    if (personagemVivo) {
        
        personagemVivo = false;
        pararSomGameTheme()
        somGameOver()
        alteraSpritePersonagemMorto()
        clearInterval(timerCowntdown)
        clearInterval(movimentacao)
        clearInterval(movimentarDownGrade)
        clearInterval(moveMonstros)
        mostrarPontuacao()
        monstrosEmCampo.forEach(monstroEmCampo => {
            alteraSpriteMonstroParado(monstroEmCampo.divId.id)
        })
    
        if (tipoFim == 'morreu' || tipoFim == 'tempo') {
            
            salvaVidasAoMorrer()
            salvaPontoAoGanhar()
            salvaTempoAoGanhar()
            salvaFaseAoPerder()
    
        } else if (tipoFim == 'fimDeJogo') {
    
            tempoTotal = tempoTotal + (tempoInicialPartida - tempoPartida);
            localStorageReset()
            rankingCadastro(pontuacaoTotal,tempoTotal,usuarioLogado)
            
        }
        
        setTimeout(() => {
            gameOver(tipoFim)
            mudaPagina(tipoFim)
        }, 2000)
    }

}

function mudaPagina(tipoFim) {
    if (tipoFim == 'morreu' || tipoFim == 'tempo') {

        setTimeout(() => {
            window.location.reload()
        }, 3000)

    } else if (tipoFim == 'fimDeJogo') {

        setTimeout(() => {
            window.open("index.html","_self")
        }, 3000)

    }
}
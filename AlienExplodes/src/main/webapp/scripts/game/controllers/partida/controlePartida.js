function populaGameBar() {
    document.getElementById("spanPowerBomb").innerHTML = powerBomb.atual;
    document.getElementById("spanPowerFire").innerHTML = powerFire.atual;
    document.getElementById("spanPowerSpeed").innerHTML = powerSpeed.atual;

    let minutos = Math.floor(tempoPartida / 60000);
    let segundos = ((tempoPartida % 60000) / 1000).toFixed(0);
    tempo =  `${minutos}:${(segundos < 10 ? '0' : '') + segundos}`;
    document.getElementById("spanTempo").innerHTML = tempo;
}
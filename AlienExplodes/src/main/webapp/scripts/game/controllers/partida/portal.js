function sortiarPortal(){
    var quantidadeBlocosQuebraveis = arrayObjsObstaculosDestrutiveis.length;
    var numMuro = Math.floor(Math.random()*quantidadeBlocosQuebraveis);
    var muroSortiado = arrayObjsObstaculosDestrutiveis[numMuro].id;
    return muroSortiado;
}

function aparecerPortal(divRecebe){
    let portal = document.createElement("div");
    portal.classList.add("portal");
    portal.setAttribute("id","portal");
    divRecebe.appendChild(portal);
    apareceuPortal = true;
}
function iniciaPartida() {

    //popula usuario logado
    usuarioLogado = localStorage.getItem("usuarioLogado");

    //preenche quantas vidas o usuario possui
    pegaVidas();
    document.getElementById("vidas").innerHTML = vidasRestantes;

    //popula pontos
    pegaPontoAoGanhar();

    //popula total de tempo gasto
    pegaTempoAoGanhar();

    //popula power ups
    pegaPowerUps();

    //popula a barra de informações
    populaGameBar();

    //popula o solo com os blocos
    let divBlocos = geraGradeView();

    populaTabelaComInquebraveis(divBlocos.getAttribute("id"));
                
    populaTabelaComQuebraveis("tabelaPosicoes");

    //sortia o bloco que vai aparecer portal
    portal = sortiarPortal();
    
    //gera monstros na dificuldade escolhida
    dificuldade = localStorage.getItem("dificuldade");
    geraMonstros(dificuldade);

    // Mostra qual estágio o jogador está
    pegaFase();

    // Define qual é a fase e depois de 3 segundos oculta a informação
    faseView();

    // Toca a música de start
    somGameStart();
    
    // Após mostrar o estágio, começa o jogo depois de 3 segundos
    setTimeout(() => {

        // Toca a música do jogo
        somGameTheme();

        // Remove o gif do personagem e coloca imagem estática ao inicar a partida
        document.getElementById('personagem').style.backgroundImage = 'url(./imagens/sprites/personagem/frente.png)';

        //Habilita o movimento do personagem e dos monstros
        eventListeners();
        movimentar();
        controleMovimentoMonstros();
    
        //Inicia o timer
        timerPartida();
    }, 3000);

}
class ControleGeracaoBombas{

    constructor(){
        this.bloco = 50;
        this.limiteBombas = powerBomb.atual;
    }

    resetGeracao() {
        this.limiteBombas = 1;    
    }

    colocaBomba(personagem) {
         
        let bombasEmCampo = parseInt(document.getElementById("bombasEmCampo").innerHTML);
        let bombasColocadas = parseInt(document.getElementById("bombasColocadas").innerHTML);


        if (bombasEmCampo < this.limiteBombas) {  
            
            let restoTop = parseInt(personagem.style.top.replace("px", "")) / this.bloco ;
            let restoLeft = parseInt(personagem.style.left.replace("px", "")) / this.bloco ;

            let linhaBomba;
            let celulaBomba;

            if ((restoTop - parseInt(restoTop)) == 0 || (restoTop - parseInt(restoTop)) < 0.5) {

                linhaBomba = document.getElementById("l" + parseInt(restoTop  + 1));

            }else if((restoTop - parseInt(restoTop)) > 0.5) {

                linhaBomba = document.getElementById("l" + (parseInt(restoTop  + 2)));

            }

            let colunalinhaBomba = linhaBomba.children;

            if ((restoLeft - parseInt(restoLeft)) == 0 || (restoLeft - parseInt(restoLeft)) < 0.5) {
                
                celulaBomba = colunalinhaBomba[parseInt(restoLeft)];

            }else if((restoLeft - parseInt(restoLeft)) > 0.5) {

                celulaBomba = colunalinhaBomba[parseInt(restoLeft) + 1];

            }
            

            if (celulaBomba.children.length == 0) {

                bombasColocadas++;
                document.getElementById("bombasColocadas").innerHTML = bombasColocadas; 

                let retornoGeraBombaView = GeraBombaView.geraBomba(celulaBomba, bombasColocadas);
                bombasEmCampo++;
                document.getElementById("bombasEmCampo").innerHTML = bombasEmCampo;
                arrayBombas.push({id:(retornoGeraBombaView.id)});
                colisaoBombas = false;
                
                return retornoGeraBombaView;
            }
        }
    }
}
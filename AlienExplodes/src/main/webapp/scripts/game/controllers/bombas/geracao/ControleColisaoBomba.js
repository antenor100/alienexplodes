class ControleColisaoBomba {

    constructor() {}

    static colisaoBomba(bomba) {

        let aRect = document.getElementById('personagem').getBoundingClientRect();
        let bRect = bomba.getBoundingClientRect();
        
        let rect1 = {
                        top: aRect.top,
                        left: aRect.left
                    }

        let rect2 = {
                        top: bRect.top,
                        left: bRect.left
                    }

        if ((
            ((rect1.top + aRect.height/1.25) <= (rect2.top)) ||
            (rect1.top >= (rect2.top + bRect.height/1.25)) ||
            ((rect1.left + aRect.width/1.25) <= rect2.left) ||
            (rect1.left >= (rect2.left + bRect.width/1.25))
            )) {
            colisaoBombas = true;
            clearInterval(colisaoBomba);
        }
    }
}
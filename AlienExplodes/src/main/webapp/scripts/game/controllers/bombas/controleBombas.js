function controleBombas() {

    divPersonagem = document.getElementById("personagem");

    if(conGeracao == undefined){
        conGeracao =  new ControleGeracaoBombas();
    }
    let bombasEmCampo = parseInt(document.getElementById("bombasEmCampo").innerHTML);

    if (bombasEmCampo < conGeracao.limiteBombas) {
        let retornoColocaBombas = conGeracao.colocaBomba(divPersonagem);

        if (retornoColocaBombas) {
            ControleTempoViewBomba.timerBomba(retornoColocaBombas);
    
            let idBomba = retornoColocaBombas.id;
    
            var colisaoBombas = () => colisaoBomba = setInterval(() => {
                ControleColisaoBomba.colisaoBomba(retornoColocaBombas);
            }, 25);
            
            colisaoBombas();
    
            setTimeout(() => {
                clearInterval(colisaoBomba)
                arrayBombas = arrayBombas.filter((elem) => {
                    return elem.id != idBomba;
                });
            }, tempoBomba);
        }
    }
}
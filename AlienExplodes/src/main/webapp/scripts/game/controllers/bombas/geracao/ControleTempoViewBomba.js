class ControleTempoViewBomba{
   
    constructor(){}

    static timerBomba(divBomba) {
            
        setTimeout(function () {

            let tdBomba = divBomba.parentElement;

            if (tdBomba != null) {

                divBomba.remove();           
                document.getElementById("bombasEmCampo").innerHTML = (parseInt(document.getElementById("bombasEmCampo").innerHTML) - 1);
    
                controleExplosao(tdBomba);
            }

        }, tempoBomba);
    }
}
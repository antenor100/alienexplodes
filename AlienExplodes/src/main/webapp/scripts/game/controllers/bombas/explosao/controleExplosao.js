function controleExplosao(divBomba) {

    if (personagemVivo) {

        if(conAlcance == undefined){
            conAlcance = new ControleAlcanceExplosao();
        }
    
        conAlcance.resetAlcance();
        
        let retornoConExplosao =  ControleAlcanceExplosao.geraDivsExplosao(divBomba, conAlcance);
        
        let divsExplosao = GeraExplosaoView.preencheExplosao(retornoConExplosao);
    
        somExplosao();
    
        controleMorteMonstro(divsExplosao);
    
        let morreu = controleMortePersonagem(divsExplosao, 'explosoes');
    
        if (!morreu) {
            setTimeout(() => {
                // controleMorteMonstro(divsExplosao);
                controleMortePersonagem(divsExplosao);
            }, 350);
        }
    
        ControleTempoViewExplosao.removeExplosao(divsExplosao);
    }
}

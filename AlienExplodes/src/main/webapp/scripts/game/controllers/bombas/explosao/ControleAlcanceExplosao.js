class ControleAlcanceExplosao{

    constructor(){
        this.geraPra = {
            U:{continuaVerificacao:true},
            D:{continuaVerificacao:true},
            L:{continuaVerificacao:true},
            R:{continuaVerificacao:true},
        };
        
        this.muros = arrayObjsObstaculosFixos.concat(arrayObjsObstaculosDestrutiveis).concat(arrayBombas);

        this.alcanceExplosao = powerFire.atual;
    }
    //Reseta condições de verificacao
    resetAlcance(){
        this.geraPra.U.continuaVerificacao = true;
        this.geraPra.D.continuaVerificacao = true;
        this.geraPra.L.continuaVerificacao = true;
        this.geraPra.R.continuaVerificacao = true;
    }

    static geraDivsExplosao(tdBomba, conAlcance){     
        let tdElement = tdBomba;
        let trElement = tdElement.parentElement;
        let idTd = tdElement.id;
        let idTr = trElement.id;
        
        //[0] é o div central
        let divAhPreencher = [{div:tdElement,direcao:"N"}];

        for (let i = 1; i <= conAlcance.alcanceExplosao; i++) {         
            if(conAlcance.geraPra.U.continuaVerificacao){
                divAhPreencher.push(this.pegaProximoTd(idTr,idTd,"U",i,conAlcance));
            }
            if(conAlcance.geraPra.D.continuaVerificacao){
                divAhPreencher.push(this.pegaProximoTd(idTr,idTd,"D",i,conAlcance));
            }
            if(conAlcance.geraPra.L.continuaVerificacao){
                divAhPreencher.push(this.pegaProximoTd(idTr,idTd,"L",i,conAlcance));
            }
            if(conAlcance.geraPra.R.continuaVerificacao){
                divAhPreencher.push(this.pegaProximoTd(idTr,idTd,"R",i,conAlcance));
            }   
        }

        let arrayObjFiltrado = divAhPreencher.filter( i => (i.div != "" && i.div !== undefined));

        return {alcance:conAlcance.alcanceExplosao,arrayObjs:arrayObjFiltrado};
    }


    static pegaProximoTd(idTr,idTd,dir,dis,conAlcance){
        let numeroTr;
        let numeroTd;
        //Seta numero tr e td de acordo com a direção
        if (dir == "U") {         
            numeroTr = parseInt(idTr.substring(1,idTr.lenght)) - dis;
            numeroTd = parseInt(idTd.substring(1,idTd.lenght));
        } else if (dir == "D") {
            numeroTr = parseInt(idTr.substring(1,idTr.lenght)) + dis;
            numeroTd = parseInt(idTd.substring(1,idTd.lenght));
        } else if(dir == "L") {            
            numeroTr = parseInt(idTr.substring(1,idTr.lenght));
            numeroTd = parseInt(idTd.substring(1,idTd.lenght)) - dis;
        } else if (dir == "R") {
            numeroTr = parseInt(idTr.substring(1,idTr.lenght));
            numeroTd = parseInt(idTd.substring(1,idTd.lenght)) + dis;
        }    
        //Se tr e td obdece todos os limites
        if (numeroTr != null && numeroTr > 0 && numeroTr <= 11 && numeroTd != null && numeroTd > 0 && numeroTd <= 19) {
            //Pega os filhos da linha
            let newTrChildren = document.getElementById("l" + numeroTr).children;
            //Se div não contem nenhum item do array pode popular com a explosao
            if (!this.seContemBloco(newTrChildren[numeroTd - 1])) {
                return {div:newTrChildren[numeroTd - 1],direcao:dir};                  
            //Se div contem algum dos muros essa direção não precisa ser verificada denovo
            }else{
                //Condicionais para vetar a direção certa
                if(dir == "U")conAlcance.geraPra.U.continuaVerificacao = false;
                if(dir == "D")conAlcance.geraPra.D.continuaVerificacao = false;
                if(dir == "L")conAlcance.geraPra.L.continuaVerificacao = false;
                if(dir == "R")conAlcance.geraPra.R.continuaVerificacao = false;
                return "";
            }       
        }else{
            //Condicionais para vetar a direção certa
            if(dir == "U")conAlcance.geraPra.U.continuaVerificacao = false;
            if(dir == "D")conAlcance.geraPra.D.continuaVerificacao = false;
            if(dir == "L")conAlcance.geraPra.L.continuaVerificacao = false;
            if(dir == "R")conAlcance.geraPra.R.continuaVerificacao = false;
            return "";
        }
    }

    //Se contem bloco é responsavel por ver se o div contem um bloco ou não.
    //Está sendo muito aproveitado para implementar regras na explosao
    //!!Originalmente esse metodo foi criado apenas para retornar se o div passado contem qualquer um dos muros concatenados
    static seContemBloco(tdElement){
        //É necessario instanciar o conAlcance novamente para pegar as bombas atualizadas
        let conAlcance = new ControleAlcanceExplosao();
        //Pega muros
        let arrayMuros = conAlcance.muros;
        //Concatena portal e powerups no array de muros
        if (apareceuPortal) {
            arrayMuros = arrayMuros.concat(document.getElementById('portal'));
        }
        if (powerUps.length > 0) {
            arrayMuros = arrayMuros.concat(powerUps);
        }
        //Variavel responsavel em popular ou não o div
        let tdContainMuro = false;   
        //iteração de todos os muros concatenados na parte superior do fonte
        arrayMuros.forEach(i => {
            let muroIteracao = document.getElementById(i.id);
            if (tdElement.contains(muroIteracao)) {
                tdContainMuro = true; 

                //Regras com muros quebraveis
                if (parseInt(muroIteracao.id.replace("muro",""))  > 45) {
                    //Sorteia porup caso não seja portal
                    if (muroIteracao.id != portal) {
                        setTimeout(() => {
                            sortiarPowerUp(muroIteracao.parentElement);
                        }, 350);
                    }
                    //regra para mostrar portal
                    if(muroIteracao.id == portal){
                        let localMuroIteracao = muroIteracao.parentElement;
                        muroIteracao.style.filter = 'invert(75%)';
                        setTimeout(() => {
                            muroIteracao.remove();
                            aparecerPortal(localMuroIteracao);
                        }, 350);
                    }
                    else{
                        muroIteracao.style.filter = 'invert(75%)';
                        setTimeout(() => {
                            muroIteracao.remove();
                        }, 350);
                    }
                    //Regra para explodir bloco destrutivel
                    arrayObjsObstaculosDestrutiveis.forEach((i)=>{
                        if (i.id == muroIteracao.id) {
                            arrayObjsObstaculosDestrutiveis.splice(arrayObjsObstaculosDestrutiveis.indexOf(i),1);
                        }
                    });
                }
                //regras explosao de outra bomba
                if (i.id.substring(0, 5) == "bomba") {
                    let bomba = document.getElementById(i.id);
                    let tdBomba = bomba.parentElement;
                    bomba.remove();          
                    document.getElementById("bombasEmCampo").innerHTML = (parseInt(document.getElementById("bombasEmCampo").innerHTML) - 1);
                    controleExplosao(tdBomba);
                //regras para powerups
                } else if (i.id.substring(0, 5) == "power") {
                    document.getElementById(i.id).remove();
                    for (let index = 0; index < powerUps.length; index++) {
                        if (powerUps[index].id == i.id) {
                            powerUps.splice(index, 1);
                        }
                    }
                //regra para downgrade
                } else if (i.id == "downGrade") {
                    document.getElementById(i.id).remove();
                    for (let index = 0; index < powerUps.length; index++) {
                        if (powerUps[index].id == i.id) {
                            powerUps.splice(index, 1);
                        }
                    }
                }
            }
        });

        //retorno final do se contem blocos para saber se vai popular ou não
        if (tdContainMuro) {
            return true;
        }else{
            return false;
        }       
    }
}
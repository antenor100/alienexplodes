function controleMortePersonagem(divsComparacao, qualArray) {
    
    let morreu = false;

    if (qualArray == 'explosoes') {
        
        for (let i = 0; i < divsComparacao.length; i++) {
    
            morreu = analisaColisaoSemMov([divsComparacao[i]],document.getElementById("personagem"));
    
            if (morreu) {
                break;
            }
        }
    } else if (qualArray == 'monstros') {

        for (let i = 0; i < divsComparacao.length; i++) {
    
            morreu = analisaColisaoSemMovMonstros([divsComparacao[i]],document.getElementById("personagem"));
    
            if (morreu) {
                break;
            }
        }
    }


    if (morreu) {
        if (vidasRestantes > 1) {
            finalizaPartida('morreu');
        } else {
            finalizaPartida('fimDeJogo');
        }
    }

    return morreu;
}
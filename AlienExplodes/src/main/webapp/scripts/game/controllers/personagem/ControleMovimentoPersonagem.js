class ControleMovimentoPersonagem{

    constructor(tamanhoPasso, idPersonagem){
        this.tamanhoPasso = tamanhoPasso
        this.idPersonagem = idPersonagem;
    }

    movePersonagem(eventDown) {

        let pixels = this.tamanhoPasso;

        if (eventDown == 'L') {

            this.movePra(this.idPersonagem, "L", pixels);

        }else if (eventDown == 'U') {

            this.movePra(this.idPersonagem, "U", pixels);

        }else if (eventDown == 'R') {

            this.movePra(this.idPersonagem, "R", pixels);

        }else if (eventDown == 'D') {

            this.movePra(this.idPersonagem, "D", pixels);
        }  
        
    }
        
    movePra(idElement, pra, pixels) {
        
        let personagem = document.getElementById(idElement);

        if (pra == "U") {
            personagem.style.top = parseInt(personagem.style.top) - pixels + "px";
        }else if (pra == "D") {
            personagem.style.top = parseInt(personagem.style.top) + pixels + "px";
        }else if (pra == "L") {
            personagem.style.left = parseInt(personagem.style.left) - pixels + "px";
        }else if (pra == "R") {
            personagem.style.left = parseInt(personagem.style.left) + pixels + "px";
        }
    }

    static defineUltimoMovimento(teclaPressionada){
        document.getElementById("ultimoMovimento").innerHTML = teclaPressionada;
    }
}
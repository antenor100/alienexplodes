function controleMorteMonstro(divsExplosao) {
    
    let divsMonstros = []; 
    
    monstrosEmCampo.forEach(i => {
        divsMonstros.push(i.divId);
    });
    
    let morreu = false;

    for (let i = 0; i < divsMonstros.length; i++) {
        morreu = analisaColisaoSemMov(divsExplosao, divsMonstros[i]);
        if (morreu) {
            monstrosEmCampo.forEach(j => {
                if (j.divId == divsMonstros[i]) {
                    j.tipo.quantidadeDeBombasParaMorrer--;
                    if (j.tipo.quantidadeDeBombasParaMorrer == 0) {
                        alteraSpriteMonstroMorto(divsMonstros[i]);
                        monstrosEmCampo.splice(monstrosEmCampo.indexOf(j),1);
                        //Posiivel implementacao de score com tipo de monstro e fando push em monstrosMortos
                        pontuar(j.tipo.pontuacao);
                    } else {
                        document.getElementById(j.divId.id).classList.remove('huerotateVermelho')
                        document.getElementById(j.divId.id).classList.add('grayscale')
                        setTimeout(() => {
                            document.getElementById(j.divId.id).classList.remove('grayscale')
                            document.getElementById(j.divId.id).classList.add('huerotateRosa')
                        }, 350);
                    }
                }
            });
        }
    }
}
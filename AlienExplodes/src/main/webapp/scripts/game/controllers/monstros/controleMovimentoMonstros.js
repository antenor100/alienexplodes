function controleMovimentoMonstros(){
    let monsterStep = 1;
    let ultimoMovMonstros = [];
    let inversoPosicoes = [
        {direcao:"U", inverso:"D"},
        {direcao:"D", inverso:"U"},
        {direcao:"L", inverso:"R"},
        {direcao:"R", inverso:"L"}
    ];
    
    var moverMonstros = () => moveMonstros = setInterval(() => {

        monstrosEmCampo.forEach(monstroEmCampo => {
            let possiveisCaminhos = [];
            let ultimoMovSelecionado;
            let inverso;

            if (!verificaColisaoDosMonstros(monstroEmCampo.divId.id, monsterStep, "U")) possiveisCaminhos.push("U");
            if (!verificaColisaoDosMonstros(monstroEmCampo.divId.id, monsterStep, "D")) possiveisCaminhos.push("D");
            if (!verificaColisaoDosMonstros(monstroEmCampo.divId.id, monsterStep, "L")) possiveisCaminhos.push("L");
            if (!verificaColisaoDosMonstros(monstroEmCampo.divId.id, monsterStep, "R")) possiveisCaminhos.push("R");

            if (possiveisCaminhos.length > 0) {
                
                if (ultimoMovMonstros.length > 0) {
                    ultimoMovMonstros.forEach(ultimoMovMonstro => {
                        if (monstroEmCampo.divId.id == ultimoMovMonstro.monstro) {
                            ultimoMovSelecionado = ultimoMovMonstro;
                            inversoPosicoes.forEach(posicao => {
                                if (ultimoMovMonstro.direcao == posicao.direcao) {
                                    if (possiveisCaminhos.includes(ultimoMovMonstro.direcao)) {
                                        inverso = posicao.inverso;
                                        let indexInverso = possiveisCaminhos.indexOf(inverso);
                                        possiveisCaminhos.splice(indexInverso,1);
                                    }  
                                }
                            }); 
                        }
                    });  
                }

                let posSorteada = Math.floor(Math.random() * (possiveisCaminhos.length - 1 + 1) + 1);
                
                let existeRegistro = false;

                ultimoMovMonstros.forEach(monstroIteracao => {
                    if (monstroIteracao.monstro == monstroEmCampo.divId.id) {
                        monstroIteracao.direcao = possiveisCaminhos[posSorteada - 1];
                        existeRegistro = true;
                    } 
                });
                
                if (!existeRegistro) {
                    ultimoMovMonstros.push({monstro:monstroEmCampo.divId.id, direcao:possiveisCaminhos[posSorteada - 1]});
                }

                if(ultimoMovMonstros.length == monstrosEmCampo.length){
                    if(monstroEmCampo.tipo.tipo == 2){
                        let sorteio = Math.floor(Math.random() * (100 + 1) + 1);
                        if (sorteio < 3) {
                            possiveisCaminhos[posSorteada - 1] = inverso;
                            ultimoMovMonstros.forEach(movAhMudar => {
                                if (movAhMudar == ultimoMovSelecionado) {
                                    movAhMudar.direcao = inverso;
                                }
                            });
                        }
                    }else if (monstroEmCampo.tipo.tipo == 3){
                        let sorteio = Math.floor(Math.random() * (100 + 1) + 1);
                        if (sorteio < 5) {
                            possiveisCaminhos[posSorteada - 1] = inverso;
                            ultimoMovMonstros.forEach(movAhMudar => {
                                if (movAhMudar == ultimoMovSelecionado) {
                                    movAhMudar.direcao = inverso;
                                }
                            });
                        }
                    }
                }

                moveMonstro(possiveisCaminhos[posSorteada - 1], (monsterStep * monstroEmCampo.tipo.velocidade), monstroEmCampo.divId.id);
            } else {
                alteraSpriteMonstroParado(monstroEmCampo.divId.id);
            }
        });
        
    }, 35);

    moverMonstros();
}

function moveMonstro(eventDown, monsterStep, idMonstro) {

    if (eventDown == 'L') {

        movePra(idMonstro, "L", monsterStep);

    }else if (eventDown == 'U') {

        movePra(idMonstro, "U", monsterStep);

    }else if (eventDown == 'R') {

        movePra(idMonstro, "R", monsterStep);

    }else if (eventDown == 'D') {

        movePra(idMonstro, "D", monsterStep);
    }  
    
}
    
function movePra(idMonstro, pra, monsterStep) {
    
    let monstro = document.getElementById(idMonstro);

    if (pra == "U") {
        monstro.style.top = parseInt(monstro.style.top) - monsterStep + "px";
        alteraSpriteMonstroAndando(pra, idMonstro);
    }else if (pra == "D") {
        monstro.style.top = parseInt(monstro.style.top) + monsterStep + "px";
        alteraSpriteMonstroAndando(pra, idMonstro);
    }else if (pra == "L") {
        monstro.style.left = parseInt(monstro.style.left) - monsterStep + "px";
        alteraSpriteMonstroAndando(pra, idMonstro);
    }else if (pra == "R") {
        monstro.style.left = parseInt(monstro.style.left) + monsterStep + "px";
        alteraSpriteMonstroAndando(pra, idMonstro);
    }
}
function geraMonstros(dificuldade){//possiveis tipos facil, medio e dificil. Estão ligados com o model dos monstros
   
    let quantidadeMonstros; //Quantos monstros a fase receberá
    let quantidadeMonstroPorTipo; //Quantidade por tipo de monstros

    if (dificuldade == "facil") {
       quantidadeMonstros = 5;
       quantidadeMonstroPorTipo = {"tipo1":5, "tipo2":0, "tipo3":0};
    }else if (dificuldade == "medio") {
        quantidadeMonstros = 6;
        quantidadeMonstroPorTipo = {"tipo1":3, "tipo2":3, "tipo3":0};
    }else if (dificuldade == "dificil"){
        quantidadeMonstros = 7;
        quantidadeMonstroPorTipo = {"tipo1":0, "tipo2":4, "tipo3":3};
    }else{
        console.log("Tipo do monstro especificado " + dificuldade +" não é aceito");  
    }
    
    let grade = document.getElementById("tabelaPosicoes");
    let arrayElementsDisponiveis = [];//Array com todas as possiveis celulas da grade que os monstros podem ser spawnados


    let linhas = grade.children;

    for(let elementLinha of linhas){ //Esquema de for's para popular arrayElementsDisponiveis
        let colunasLinhaIteracao = elementLinha.children;
        
        for(let elementColuna of colunasLinhaIteracao){
            if (elementColuna.children.length == 0) {
                if (elementLinha.id == "l1" && elementColuna.id == "c1") {}
                else if(elementLinha.id == "l1" && elementColuna.id == "c2") {}
                else if(elementLinha.id == "l1" && elementColuna.id == "c3") {}
                else if(elementLinha.id == "l1" && elementColuna.id == "c4") {}
                else if(elementLinha.id == "l1" && elementColuna.id == "c5") {}
                else if(elementLinha.id == "l2" && elementColuna.id == "c1") {}
                else if(elementLinha.id == "l2" && elementColuna.id == "c3") {}
                else if(elementLinha.id == "l2" && elementColuna.id == "c5") {}
                else if(elementLinha.id == "l3" && elementColuna.id == "c1") {}
                else if(elementLinha.id == "l3" && elementColuna.id == "c2") {}
                else if(elementLinha.id == "l3" && elementColuna.id == "c3") {}
                else if(elementLinha.id == "l3" && elementColuna.id == "c4") {}
                else if(elementLinha.id == "l3" && elementColuna.id == "c5") {}
                else if(elementLinha.id == "l4" && elementColuna.id == "c1") {}
                else if(elementLinha.id == "l4" && elementColuna.id == "c3") {}
                else if(elementLinha.id == "l4" && elementColuna.id == "c5") {}
                else if(elementLinha.id == "l5" && elementColuna.id == "c1") {}
                else if(elementLinha.id == "l5" && elementColuna.id == "c2") {}
                else if(elementLinha.id == "l5" && elementColuna.id == "c3") {}
                else if(elementLinha.id == "l5" && elementColuna.id == "c4") {}
                else if(elementLinha.id == "l5" && elementColuna.id == "c5") {}
                else{
                    arrayElementsDisponiveis.push(elementColuna);
                }
            }
        }
    }

    let arrayElementsPosicoesMonstros = []; //Array que contem os elementos aleatorizados para receber o numero de monstros setados

    for (let i = 0; i < quantidadeMonstros; i++) { //For para popular de forma aleatoria arrayElementsPosicoesMonstros de acordo com o numero total de monstros
        let posicaoAleatoriaDePosicoesDisponiveis = Math.floor(Math.random() * (arrayElementsDisponiveis.length - 0 + 1) + 0);
        arrayElementsPosicoesMonstros.push(arrayElementsDisponiveis[posicaoAleatoriaDePosicoesDisponiveis]);
        arrayElementsDisponiveis.splice(posicaoAleatoriaDePosicoesDisponiveis,1);
    }
    
    let arrayObjPreencherComTipo = [];

    for (let i = 0; i < quantidadeMonstroPorTipo.tipo1; i++) { //For para popular o array de objetos que contem o tipo e o local aleatorio de spawn dos monstros faceis
        let posicaoSorteada = Math.floor(Math.random() * ((arrayElementsPosicoesMonstros.length - 1) - 0 + 1) + 0);
        arrayObjPreencherComTipo.push({tipo: JSON.parse(JSON.stringify(monstrosTipoUm)), divId: arrayElementsPosicoesMonstros[posicaoSorteada]});
        arrayElementsPosicoesMonstros.splice(posicaoSorteada,1);
    } 

    for (let i = 0; i < quantidadeMonstroPorTipo.tipo2; i++) { //For para popular o array de objetos que contem o tipo e o local aleatorio de spawn dos monstros medios
        let posicaoSorteada = Math.floor(Math.random() * ((arrayElementsPosicoesMonstros.length - 1) - 0 + 1) + 0);
        arrayObjPreencherComTipo.push({tipo: JSON.parse(JSON.stringify(monstrosTipoDois)), divId: arrayElementsPosicoesMonstros[posicaoSorteada]});
        arrayElementsPosicoesMonstros.splice(posicaoSorteada,1);
    } 

    for (let i = 0; i < quantidadeMonstroPorTipo.tipo3; i++) { //For para popular o array de objetos que contem o tipo e o local aleatorio de spawn dos monstros dificeis
        let posicaoSorteada = Math.floor(Math.random() * ((arrayElementsPosicoesMonstros.length - 1) - 0 + 1) + 0);
        arrayObjPreencherComTipo.push({tipo: JSON.parse(JSON.stringify(monstrosTipoTres)), divId: arrayElementsPosicoesMonstros[posicaoSorteada]});
        arrayElementsPosicoesMonstros.splice(posicaoSorteada,1);
    }
    
    let monstros = geraMonstrosView(arrayObjPreencherComTipo);

    for (let i = 0; i < monstros.length; i++) {
        arrayObjPreencherComTipo[i].divId = monstros[i];
        monstrosEmCampo.push(arrayObjPreencherComTipo[i]);
    }
}
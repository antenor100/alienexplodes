function movePersonagemComColisao(event, passo, idPersonagem) {

    let teclaPresionada = event;

    let conMov = new ControleMovimentoPersonagem(passo, idPersonagem);

    let vaiColidir = verificaColisao(idPersonagem, passo, teclaPresionada);
    
    if (!vaiColidir) {
        conMov.movePersonagem(event);
        alteraSpritePraDirecao(event, 'personagem')
    }  else {
        analisaColisaoComMargem(idPersonagem, passo, teclaPresionada, conMov);
    }
}

function verificaColisao(idPersonagem, passo, teclaPresionada) {
    for(let i = 0; i < arrayObjsObstaculosFixos.length; i++){
    
        let ehcolisaoFixos = analisaColisaoRetanguloMov(idPersonagem, arrayObjsObstaculosFixos[i].id, passo, teclaPresionada);

        if (ehcolisaoFixos) {     
            return true;
        }
    }

    for(let i = 0; i < arrayObjsObstaculosDestrutiveis.length; i++){

        let ehcolisaoQuebraveis = false;

        if (!(document.getElementById(arrayObjsObstaculosDestrutiveis[i].id) == null)) {
            ehcolisaoQuebraveis = analisaColisaoRetanguloMov(idPersonagem, arrayObjsObstaculosDestrutiveis[i].id, passo, teclaPresionada);
        }

        if (ehcolisaoQuebraveis) {   
            return true;
        }
    }

    if (colisaoBombas) {
        for (let i = 0; i < arrayBombas.length; i++) {
    
            let ehcolisaoBombas = false;
    
            if (!(document.getElementById(arrayBombas[i].id) == null)) {
                ehcolisaoBombas = analisaColisaoRetanguloMov(idPersonagem, arrayBombas[i].id, passo, teclaPresionada);
            }
    
            if (ehcolisaoBombas) {   
                return true;
            }
        }
    }

    powerUps.forEach((element, index) => {
        let arrayPowerUnic = [element];
        let colidePowerUp = analisaColisaoSemMov(arrayPowerUnic,document.getElementById(idPersonagem));
        if(colidePowerUp) {
            element.remove();
            powerUps.splice(index, 1);
            power(element.id);
        }
    });

    //Colisao Portal =>
    if(apareceuPortal){
        let divPortal = document.getElementById("portal");
        let divPersonagem = document.getElementById(idPersonagem);
        let colidePortal = analisaColisaoSemMov([divPortal], divPersonagem);
        if(colidePortal && monstrosEmCampo.length == 0){
            proximaPartida();
        }
    }

}

function verificaColisaoDosMonstros(idMonstro, passo, teclaPresionada) {
    for(let i = 0; i < arrayObjsObstaculosFixos.length; i++){
    
        let ehcolisaoFixos = analisaColisaoRetanguloMov(idMonstro, arrayObjsObstaculosFixos[i].id, passo, teclaPresionada);

        if (ehcolisaoFixos) {     
            return true;
        }
    }

    for(let i = 0; i < arrayObjsObstaculosDestrutiveis.length; i++){

        let ehcolisaoQuebraveis = false;

        if (!(document.getElementById(arrayObjsObstaculosDestrutiveis[i].id) == null)) {
            ehcolisaoQuebraveis = analisaColisaoRetanguloMov(idMonstro, arrayObjsObstaculosDestrutiveis[i].id, passo, teclaPresionada);
        }

        if (ehcolisaoQuebraveis) {   
            return true;
        }
    }

    for (let i = 0; i < arrayBombas.length; i++) {

        let ehcolisaoBombas = false;

        if (!(document.getElementById(arrayBombas[i].id) == null)) {
            ehcolisaoBombas = analisaColisaoRetanguloMov(idMonstro, arrayBombas[i].id, passo, teclaPresionada);
        }

        if (ehcolisaoBombas) {   
            return true;
        }
    }
}


var movimentarDownGrade = () => movimentacaoDownGrade = setInterval(() => {
    var divPersonagem = document.getElementById('personagem');    
    sendoUsado = 1;
    let arrayMonstrosEmCampo = [];

    monstrosEmCampo.forEach(i => {
        arrayMonstrosEmCampo.push(i.divId);
    });

    let morreu = controleMortePersonagem(arrayMonstrosEmCampo, 'monstros');

    if (!morreu) {
        if (movingRight) {
            movePersonagemComColisao('L', step, divPersonagem.id)
        }
        if (movingDown) {
            movePersonagemComColisao('U', step, divPersonagem.id)
        }
        if (movingLeft) {
            movePersonagemComColisao('R', step, divPersonagem.id)
        }
        if (movingUp) {
            movePersonagemComColisao('D', step, divPersonagem.id)
        }
        if (!movingRight && !movingDown && !movingLeft && !movingUp) {
            alteraSpritePersonagemParado('personagem')
        }
    }
    mostrarPontuacao()
}, speed)

var movimentar = () => movimentacao = setInterval(() => {
    var divPersonagem = document.getElementById('personagem');    
    sendoUsado = 2;
    clearInterval(movimentarDownGrade);

    let arrayMonstrosEmCampo = [];

    monstrosEmCampo.forEach(i => {
        arrayMonstrosEmCampo.push(i.divId);
    });

    let morreu = controleMortePersonagem(arrayMonstrosEmCampo, 'monstros');

    if (!morreu) {
        if (movingRight) {
            movePersonagemComColisao('R', step, divPersonagem.id)
        }
        if (movingDown) {
            movePersonagemComColisao('D', step, divPersonagem.id)
        }
        if (movingLeft) {
            movePersonagemComColisao('L', step, divPersonagem.id)
        }
        if (movingUp) {
            movePersonagemComColisao('U', step, divPersonagem.id)
        }
        if (!movingRight && !movingDown && !movingLeft && !movingUp) {
            alteraSpritePersonagemParado('personagem')
        }
    }
    mostrarPontuacao()
}, speed)

function eventListeners() {
    document.body.addEventListener('keydown', (e) => {
        if (e.key == "ArrowLeft")
            movingLeft = true
        if (e.key == "ArrowUp")
            movingUp = true
        if (e.key == "ArrowRight")
            movingRight = true
        if (e.key == "ArrowDown")
            movingDown = true
    })
    
    document.body.addEventListener('keyup', (e) => {
        if (e.key == "ArrowLeft")
        movingLeft = false
        if (e.key == "ArrowUp")
        movingUp = false
        if (e.key == "ArrowRight")
        movingRight = false
        if (e.key == "ArrowDown")
        movingDown = false
    })
    
    document.body.addEventListener('keypress' , (e) => {
        if (document.getElementById("bombasEmCampo").innerHTML == 0) {
            colisaoBombas = true;
        }
        if (e.keyCode == 32 && colisaoBombas)
            controleBombas()
    })
}
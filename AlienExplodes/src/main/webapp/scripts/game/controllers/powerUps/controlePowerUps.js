var probabilidade = 0;

function sortiarPowerUp(divRecebe){
    let pdus = document.createElement("div");
    pdus.classList.add("powerUp");
    if (dificuldade == "facil") {
        var probPowerSpeed = powerSpeed.probabilidade.facil;
        var probPowerBomb = powerBomb.probabilidade.facil + probPowerSpeed;
        var probPowerFire = powerFire.probabilidade.facil + probPowerBomb;
        var probDownGrade = downGrade.probabilidade.facil + probPowerFire;
    } else if (dificuldade == "medio") {
        var probPowerSpeed = powerSpeed.probabilidade.medio;
        var probPowerBomb = powerBomb.probabilidade.medio + probPowerSpeed;
        var probPowerFire = powerFire.probabilidade.medio + probPowerBomb;
        var probDownGrade = downGrade.probabilidade.medio + probPowerFire;
    } else if (dificuldade == "dificil") {
        var probPowerSpeed = powerSpeed.probabilidade.dificil;
        var probPowerBomb = powerBomb.probabilidade.dificil + probPowerSpeed;
        var probPowerFire = powerFire.probabilidade.dificil + probPowerBomb;
        var probDownGrade = downGrade.probabilidade.dificil + probPowerFire;
    } 
    
    probabilidade = Math.floor(Math.random()*100+1);
        // powerUp Velocidade
        if(probabilidade>0 && probabilidade<=probPowerSpeed && powerSpeed.porFase<powerSpeed.maximoPorFase && powerSpeed.atual<powerSpeed.maximo){
            powerSpeed.porFase++;
            geraPowerUpView("powerSpeed", divRecebe);
        }
        // powerUp Quantidade de Bombas:
        else if(probabilidade>probPowerSpeed && probabilidade<=probPowerBomb && powerBomb.porFase<powerBomb.maximoPorFase && powerBomb.atual<powerBomb.maximo){
            powerBomb.porFase++;
            geraPowerUpView("powerBomb", divRecebe);
        }
        // powerUp Alcance da bomba:
        else if(probabilidade>probPowerBomb && probabilidade<=probPowerFire && powerFire.porFase<powerFire.maximoPorFase && powerFire.atual<powerFire.maximo){
            powerFire.porFase++;
            geraPowerUpView("powerFire", divRecebe);
        }
        // downGrade:
        else if(probabilidade>probPowerFire && probabilidade<=probDownGrade && downGrade.porFase<downGrade.maximoPorFase && downGrade.atual<downGrade.maximo){
            downGrade.porFase++;
            geraPowerUpView("downGrade", divRecebe);
        }
    }

function power(tipoPower){
    somPowerUp();
    if(tipoPower == "powerSpeed"){

        powerSpeed.atual++;
        document.getElementById('spanPowerSpeed').innerHTML = powerSpeed.atual;
        if (sendoUsado == 1) {
            clearInterval(movimentacaoDownGrade);
            speed = speed - 10;
            movimentarDownGrade();
        } else {
            clearInterval(movimentacao);
            speed = speed - 10;
            movimentar();

        }
        
    } else if(tipoPower == "powerBomb"){

        powerBomb.atual++;
        conGeracao.limiteBombas++
        document.getElementById('spanPowerBomb').innerHTML = powerBomb.atual;

    } else if(tipoPower == "powerFire"){

        powerFire.atual++;
        conAlcance.alcanceExplosao++
        document.getElementById('spanPowerFire').innerHTML = powerFire.atual;

    } else if(tipoPower == "downGrade"){

        downGrade.atual++;
        clearInterval(movimentacao);
        movimentarDownGrade();
        let tempoDownGrade = 10000;
        let spanDownGrade = document.getElementById('spanDownGrade');
        spanDownGrade.innerHTML = (tempoDownGrade/1000).toFixed(0)+"s";
        document.getElementById('divDownGrade').style.opacity = 1;

        let aparecerDownGrade = setInterval(() => {
            tempoDownGrade = tempoDownGrade - 1000;
            let segundos = Math.floor((tempoDownGrade % (1000 * 60)) / 1000);
            document.getElementById("spanDownGrade").innerHTML = segundos+"s";
        }, 1000);

        setTimeout(() => {
            document.getElementById('divDownGrade').style.opacity = 0;
            clearInterval(movimentacaoDownGrade);
            clearInterval(aparecerDownGrade);
            movimentar();
       }, tempoDownGrade);
    }
}    

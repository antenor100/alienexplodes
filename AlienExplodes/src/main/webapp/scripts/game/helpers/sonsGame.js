function somGameStart() {
    let somGameStart = new Audio("sons/game/gameStart.ogg");
    somGameStart.volume = 0.5;
    somGameStart.play();
}

function somGameTheme() {
    let musica = document.getElementById("somFundo");
    musica.volume = volume;
    musica.play();
    let aumentavolume = setInterval(() => {
        if (musica.volume < 0.5) {
            volume = parseFloat(volume) + 0.1;
            musica.volume = volume;
        } else {
            clearInterval(aumentavolume);
        }
    }, 100);
}

function pararSomGameTheme() {
    let musica = document.getElementById("somFundo");
    musica.pause();
}

function somExplosao() {
    let somExplosao = new Audio("sons/game/bombExplosion.ogg");
    somExplosao.volume = 0.5;
    somExplosao.play();
}

function somPowerUp() {
    let somPowerUp = new Audio("sons/game/powerUp.ogg");
    somPowerUp.volume = 0.5;
    somPowerUp.play();
}

function somStageClear() {
    let somStageClear = new Audio("sons/game/stageClear.ogg");
    somStageClear.volume = 0.5;
    somStageClear.play();
}

function somGameOver() {
    let somGameOver = new Audio("sons/game/gameOver.ogg");
    somGameOver.volume = 0.5;
    somGameOver.play();
}
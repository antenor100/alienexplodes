function analisaColisaoRetanguloMov(idPrimeiroElemento, idSegundoElemento, passos, eventoTecla){
    
    let aRect = document.getElementById(idPrimeiroElemento).getBoundingClientRect();
    let bRect = document.getElementById(idSegundoElemento).getBoundingClientRect();

    
    let rect1 = {
                    top: aRect.top,
                    left: aRect.left
                }

    let rect2 = {
                    top: bRect.top,
                    left: bRect.left
                }

    if (eventoTecla == 'L') {

        rect1.left = rect1.left - passos;

    }else if (eventoTecla == 'U') {

        rect1.top = rect1.top - passos;

    }else if (eventoTecla == 'R') {

        rect1.left = rect1.left + passos;

    }else if (eventoTecla == 'D') {

        rect1.top = rect1.top + passos;
    } 

    if (!(
        ((rect1.top + aRect.height) <= (rect2.top)) ||
        (rect1.top >= (rect2.top + bRect.height)) ||
        ((rect1.left + aRect.width) <= rect2.left) ||
        (rect1.left >= (rect2.left + bRect.width))
        )){
            
        return true;

    } else{

        return false;
    }

}

function analisaColisaoSemMov(divsComparacao,divComparacao) {

    let bRect = divComparacao.getBoundingClientRect();
    
    let colidiu;

    for (let i = 0; i < divsComparacao.length; i++) {
     
        let aRect = divsComparacao[i].getBoundingClientRect();

        let rect1 = {
            top: aRect.top,
            left: aRect.left
        };

        let rect2 = {
            top: bRect.top,
            left: bRect.left
        };

        if (!(
            ((rect1.top + aRect.height/1.25) <= rect2.top) ||
            (rect1.top >= (rect2.top + bRect.height/1.25)) ||
            ((rect1.left + aRect.width/1.25) <= rect2.left) ||
            (rect1.left >= (rect2.left + bRect.width/1.25))
            )){
                
            return true;
    
        } else{
    
            colidiu = false;
        }
    }
    return colidiu;
}

function analisaColisaoComMargem(idPersonagem, step, teclaPressionada, conMov) {
    let personagem = document.getElementById(idPersonagem);
    let topPersonagem = parseInt((personagem.style.top).replace('px', ''));
    let leftPersonagem = parseInt((personagem.style.left).replace('px', ''));

    if (!((topPersonagem % 50 == 0) && (leftPersonagem % 50 == 0))) {

        if (teclaPressionada == 'R') {
            if ((topPersonagem + step) % 100 == 0) {
                conMov.movePersonagem('D');
                alteraSpritePraDirecao('D', idPersonagem);
            }
            else if ((topPersonagem - step) % 100 == 0) {
                conMov.movePersonagem('U');
                alteraSpritePraDirecao('U', idPersonagem);
            }
            else if ((topPersonagem + step*2) % 100 == 0) {
                conMov.movePersonagem('D');
                alteraSpritePraDirecao('D', idPersonagem);
            }
            else if ((topPersonagem - step*2) % 100 == 0) {
                conMov.movePersonagem('U');
                alteraSpritePraDirecao('U', idPersonagem);
            }
        }
        else if (teclaPressionada == 'D') {
            if ((leftPersonagem + step) % 100 == 0) {
                conMov.movePersonagem('R');
                alteraSpritePraDirecao('R', idPersonagem);
            }
            else if ((leftPersonagem - step) % 100 == 0) {
                conMov.movePersonagem('L');
                alteraSpritePraDirecao('L', idPersonagem);
            }
            if ((leftPersonagem + step*2) % 100 == 0) {
                conMov.movePersonagem('R');
                alteraSpritePraDirecao('R', idPersonagem);
            }
            else if ((leftPersonagem - step*2) % 100 == 0) {
                conMov.movePersonagem('L');
                alteraSpritePraDirecao('L', idPersonagem);
            }
        }
        else if (teclaPressionada == 'L') {
            if ((topPersonagem + step) % 100 == 0) {
                conMov.movePersonagem('D');
                alteraSpritePraDirecao('D', idPersonagem);
            }
            else if ((topPersonagem - step) % 100 == 0) {
                conMov.movePersonagem('U');
                alteraSpritePraDirecao('U', idPersonagem);
            }
            if ((topPersonagem + step*2) % 100 == 0) {
                conMov.movePersonagem('D');
                alteraSpritePraDirecao('D', idPersonagem);
            }
            else if ((topPersonagem - step*2) % 100 == 0) {
                conMov.movePersonagem('U');
                alteraSpritePraDirecao('U', idPersonagem);
            }
        }
        else if (teclaPressionada == 'U') {
            if ((leftPersonagem + step) % 100 == 0) {
                conMov.movePersonagem('R');
                alteraSpritePraDirecao('R', idPersonagem);
            }
            else if ((leftPersonagem - step) % 100 == 0) {
                conMov.movePersonagem('L');
                alteraSpritePraDirecao('L', idPersonagem);
            }
            if ((leftPersonagem + step*2) % 100 == 0) {
                conMov.movePersonagem('R');
                alteraSpritePraDirecao('R', idPersonagem);
            }
            else if ((leftPersonagem - step*2) % 100 == 0) {
                conMov.movePersonagem('L');
                alteraSpritePraDirecao('L', idPersonagem);
            }
        }
    }
}

function analisaColisaoSemMovMonstros(divsComparacao,divComparacao) {

    let bRect = divComparacao.getBoundingClientRect();
    
    let colidiu;

    for (let i = 0; i < divsComparacao.length; i++) {
     
        let aRect = divsComparacao[i].getBoundingClientRect();

        let rect1 = {
            top: aRect.top,
            left: aRect.left
        };

        let rect2 = {
            top: bRect.top,
            left: bRect.left
        };

        if (!(
            ((rect1.top + 35) <= rect2.top) ||
            (rect1.top >= (rect2.top + 35)) ||
            ((rect1.left + 30) <= rect2.left) ||
            (rect1.left >= (rect2.left + 30))
            )){
                
            return true;
    
        } else{
    
            colidiu = false;
        }
    }
    return colidiu;
}
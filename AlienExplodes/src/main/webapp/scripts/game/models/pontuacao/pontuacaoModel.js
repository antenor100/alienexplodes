//Manter pontos ao reiniciar a pagina
function salvaPontoAoGanhar(){
    localStorage.setItem("pontos",pontuacaoTotal);
}

function pegaPontoAoGanhar(){
    if (localStorage.getItem("pontos") == null) {
        pontuacaoTotal = 0;
        mostrarPontuacao();
    }else{
        pontuacaoTotal = parseInt(localStorage.getItem("pontos"));
        mostrarPontuacao();
        localStorage.removeItem("pontos");
    }
}

//Manter tempo gasto ao reiniciar a pagina
function salvaTempoAoGanhar(){
    localStorage.setItem("tempo", tempoTotal + (tempoInicialPartida - tempoPartida));
}

function pegaTempoAoGanhar(){
    if (localStorage.getItem("tempo") == null) {
        tempoTotal = 0;
    }else{
        tempoTotal = parseInt(localStorage.getItem("tempo"));
        localStorage.removeItem("tempo");
    }
}

//Manter power ups ao passar de fase
function salvaPowerAoGanhar() {
    localStorage.setItem("powerUps", JSON.stringify({powerBomb: powerBomb.atual, powerFire: powerFire.atual, powerSpeed: powerSpeed.atual}))
}

function pegaPowerUps() {
    if (localStorage.getItem("powerUps") != null) {
        let localPowerUps = JSON.parse(localStorage.getItem("powerUps"));

        powerBomb.atual = localPowerUps.powerBomb;
        powerFire.atual = localPowerUps.powerFire;
        powerSpeed.atual = localPowerUps.powerSpeed;
        speed = speed - (powerSpeed.atual*10)+10;
        localStorage.removeItem("powerUps");
    }
}

//Trocar valor de vidas restantes ao recarregar a pagina
function salvaVidasAoGanhar() {
    localStorage.setItem("vidasRestantes",vidasRestantes);
}

function salvaVidasAoMorrer() {
    localStorage.setItem("vidasRestantes",vidasRestantes-1);
}

function pegaVidas() {
    if (localStorage.getItem("vidasRestantes") != null) {
        vidasRestantes = parseInt(localStorage.getItem("vidasRestantes"));
        localStorage.removeItem("vidasRestantes");
    }
}

//troca qual estágio o jogador está
function pegaFase() {
    if (localStorage.getItem("faseAtual") != null) {
        faseAtual = parseInt(localStorage.getItem("faseAtual"));
        localStorage.removeItem("faseAtual");
    }
}

function salvaFaseAoGanhar() {
    faseAtual++;
    localStorage.setItem('faseAtual', faseAtual);
}

function salvaFaseAoPerder() {
    localStorage.setItem('faseAtual', faseAtual);
}

//limpa localstorage
function localStorageReset() {
    localStorage.removeItem("pontos");
    localStorage.removeItem("tempo");
    localStorage.removeItem("vidasRestantes");
    localStorage.removeItem("dificuldade");
}
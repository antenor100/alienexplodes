var movingLeft = false;

var movingUp = false;

var movingRight = false;

var movingDown = false;

var putBomb = false;

var step = 10;

var speed = 90;

var numeroLinhas = 11;

var numeroColunas = 19;

var sendoUsado;

var portal;

var apareceuPortal = false;

var monstrosEmCampo = [];

var monstrosMortos = [];

var conAlcance;

var conGeracao;

var limiteBombas;

var movimentacao;

var moveMonstros;

var colisaoBombas = true;

var dificuldade;

var tempoInicialPartida = 300000;

var tempoBomba = 3000;

var vidasRestantes = 3;

var personagemVivo = true;

var tempoPartida = tempoInicialPartida;

var volume = 0;

var usuarioLogado;

var pontuacaoTotal;

var tempoTotal;

var faseAtual = 1;

var powerSpeed = {
    atual: 1,
    porFase: 0,
    maximoPorFase: 1,
    maximo: 5,
    probabilidade: {
        facil: 15,
        medio: 10,
        dificil: 5
    }
};

var powerBomb = {
    atual: 1,
    porFase: 0,
    maximoPorFase: 1,
    maximo: 4,
    probabilidade: {
        facil: 15,
        medio: 10,
        dificil: 5
    }
};

var powerFire = {
    atual: 1,
    porFase: 0,
    maximoPorFase: 1,
    maximo: 8,
    probabilidade: {
        facil: 15,
        medio: 10,
        dificil: 5
    }
};

var downGrade = {
    atual: 0,
    porFase: 0,
    maximoPorFase: 1,
    maximo: 999,
    probabilidade: {
        facil: 0,
        medio: 2,
        dificil: 5
    }
};
// Exemplo de requisição POST
function rankingCadastro(pontos, tempoGasto, usuarioRanking) {
    //novo httprequest
    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição: Post e a URL da API
    ajax.open("POST", "cadastroRanking", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Seta paramêtros da requisição e envia a requisição
    // let usuario = document.getElementById("nome");
    // let senha = document.getElementById("pontuacao");
    // let confirmacaoSenha = document.getElementById("tempoGasto");
    let pontuacao = pontos;
    let tempo = tempoGasto;
    let usuario = usuarioRanking;
    // ajax.send(`${usuario.name}=${usuario.value}&${senha.name}=${senha.value}&${confirmacaoSenha.name}=${confirmacaoSenha.value}&${email.name}=${email.value}`);
    ajax.send(`usuario=${usuario}&pontuacao=${pontuacao}&tempoGasto=${tempo}`);

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function() {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
            // Retorno do Ajax
            var data = ajax.responseText;
           console.log("Sucesso!");
           
        }
    }
}

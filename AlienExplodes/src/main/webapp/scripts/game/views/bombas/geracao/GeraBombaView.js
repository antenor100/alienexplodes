class GeraBombaView{

    constructor(){}

    static geraBomba(tdElement, bombasColocadas) {

        let div = document.createElement("div");
        div.setAttribute("id", "bomba" + bombasColocadas);
        div.setAttribute("class", "bomba");
        tdElement.appendChild(div);
   
        return div;
    }
}
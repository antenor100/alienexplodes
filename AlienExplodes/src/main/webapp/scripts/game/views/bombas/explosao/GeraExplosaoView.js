class GeraExplosaoView{

    constructor(){}

    static preencheExplosao(objExplosoesAlcance){
        
        let divsExplosao = [];
        let alcanceMaximo = objExplosoesAlcance.alcance;
        let arrayObjs = objExplosoesAlcance.arrayObjs;

        let contadorDirecoes = {U:0,D:0,L:0,R:0}

        arrayObjs.forEach(obj => {
            if (obj.direcao == "U") {
                contadorDirecoes.U++
                if (contadorDirecoes.U == alcanceMaximo) {
                    divsExplosao.push(this.populaDivGif(obj.div,fogoPontaCima));
                }else{
                    divsExplosao.push(this.populaDivGif(obj.div,fogoMeioCima));
                }
                
            } else if (obj.direcao == "D") {
                contadorDirecoes.D++  
                if (contadorDirecoes.D == alcanceMaximo) {
                    divsExplosao.push(this.populaDivGif(obj.div,fogoPontaBaixo));
                }else{
                    divsExplosao.push(this.populaDivGif(obj.div,fogoMeioBaixo));
                }
            } else if(obj.direcao == "L") {
                contadorDirecoes.L++              
                if (contadorDirecoes.L == alcanceMaximo) {
                    divsExplosao.push(this.populaDivGif(obj.div,fogoPontaEsquerda));
                }else{
                    divsExplosao.push(this.populaDivGif(obj.div,fogoMeioEsquerda));
                }
            } else if (obj.direcao == "R") {
                contadorDirecoes.R++  
                if (contadorDirecoes.R == alcanceMaximo) {
                    divsExplosao.push(this.populaDivGif(obj.div,fogoPontaDireita));
                }else{
                    divsExplosao.push(this.populaDivGif(obj.div,fogoMeioDireita));
                }
            } else if (obj.direcao == "N") {
                divsExplosao.push(this.populaDivGif(obj.div,fogoCentro));
            }  
        });

        return divsExplosao
    }

    static populaDivGif(div, urlGif){
        let tdExplosao = document.createElement("DIV");
        tdExplosao.classList.add("divExplosao");
        tdExplosao.style.backgroundImage = urlGif;
        div.appendChild(tdExplosao);
        return tdExplosao;
    }
}
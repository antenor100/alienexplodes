function geraGradeView() {
      
    let divBlocos = document.createElement("div");
    divBlocos.setAttribute("id","tabelaPosicoes");
    
    for(let i=1; i <= numeroLinhas; i++){
        
        let linha = document.createElement("div");
        linha.setAttribute("id", "l"+i);
        linha.classList.add("linhas");
        divBlocos.appendChild(linha);
    
        for(let j=1; j <= numeroColunas; j++){
            
            let celula = document.createElement("div");
            celula.setAttribute("id", "c"+j);
            celula.classList.add("colunas");
            linha.appendChild(celula);    
        }
    }
    
    document.getElementById("solo").appendChild(divBlocos);

    return divBlocos;
}
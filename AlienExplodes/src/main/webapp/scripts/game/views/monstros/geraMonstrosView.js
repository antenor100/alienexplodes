function geraMonstrosView(arrayObj) {
    let monstros = [];
    arrayObj.forEach(element => {
        let className1 = element.tipo.class1;
        let className2 = element.tipo.class2;
        let novoMonstro = document.createElement("div");
        let posicaoDiv = posicaoDivParaSolo(element.divId)
        let divSolo = document.getElementById("solo");
        novoMonstro.classList.add(className1);
        novoMonstro.classList.add(className2);
        novoMonstro.setAttribute("id", "monstro" + (arrayObj.indexOf(element) + 1));
        divSolo.appendChild(novoMonstro);
        novoMonstro.style.top = `${posicaoDiv.top}px`;
        novoMonstro.style.left = `${posicaoDiv.left}px`;
        monstros.push(novoMonstro);
    });
    return monstros;
}

function posicaoDivParaSolo(divEle){
    if (divEle) {
        let divPos = divEle.getBoundingClientRect();
        let soloPos = document.getElementById("solo").getBoundingClientRect();
        let posTopFinal = divPos.top - (soloPos.top);
        let posLeftFinal = divPos.left - (soloPos.left);
        return {top:posTopFinal,left:posLeftFinal};
    } else {        
        proximaPartidaBugada();
    }
}
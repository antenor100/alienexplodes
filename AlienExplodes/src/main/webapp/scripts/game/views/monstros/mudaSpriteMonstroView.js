function alteraSpriteMonstroAndando(direcao, idMonstro) {
    
    let top = document.getElementById(idMonstro).style.top;
    let left = document.getElementById(idMonstro).style.left;

    if (direcao == 'L') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/ladoEsquerdoAndando.gif)`);

    }else if (direcao == 'U') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/costaAndando.gif)`);

    }else if (direcao == 'R') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/ladoDireitoAndando.gif)`);

    }else if (direcao == 'D') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/frenteAndando.gif)`);
    }
}

function alteraSpriteMonstroParado(idMonstro) {
    let top = document.getElementById(idMonstro).style.top;
    let left = document.getElementById(idMonstro).style.left;
    let ultimaDirecao = document.getElementById(idMonstro).style.backgroundImage;
    ultimaDirecao = ultimaDirecao.substring(31, ultimaDirecao.length-13);

    if (ultimaDirecao == 'ladoEsquerdo') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/ladoEsquerdo.png)`);

    }else if (ultimaDirecao == 'costa') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/costa.png)`);

    }else if (ultimaDirecao == 'ladoDireito') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/ladoDireito.png)`);

    }else if (ultimaDirecao == 'frente') {

        document.getElementById(idMonstro).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/monstro/frente.png)`);
    }
}

function alteraSpriteMonstroMorto(divMonstro){
    divMonstro.style.filter = "grayscale(100%)";
    setTimeout(() => {
        divMonstro.remove();
    }, 200);  
}
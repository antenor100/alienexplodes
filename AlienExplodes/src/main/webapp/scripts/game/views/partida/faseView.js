function faseView() {
    let divFase = document.createElement('div');
    divFase.classList.add('faseAtual');
    let pFase1 = document.createElement('p');
    pFase1.classList.add('pFase1')
    pFase1.innerHTML = `Fase  ${faseAtual}`;
    let pFase2 = document.createElement('p');
    pFase2.classList.add('pFase2');
    pFase2.innerHTML = "Boa sorte <span class='spanBoaSorte'>!!!</span>";
    divFase.appendChild(pFase1);
    divFase.appendChild(pFase2);
    document.getElementById('faseView').appendChild(divFase);
    document.getElementById('personagem').style.background = "url('data:image/gif;base64,R0lGODlhMgAyAHcAACH5BAkyAA8ALAAAAAAyADIAgwAAABggUGgAADBIkOAgAPDoIICAgICQ4IjQ+MDAwPj4+AAAAAAAAAAAAAAAAAAAAAT/8MlJq7046827/2AojmRpnmiqrmzrvnAszw9g05sN4Jit3DKdUGcwJICx4bB43L10g8FhOo0CFAoDkgWIUgfC6CBxtLq60mo4SkY7VbY02EbGRhVW9dukOwQCBTYHdXhjbF5mJ31/gQCDCXaGkgeJil1/UFN4ZGNkRWl7ljYCAo4HP21YCm16KUKkpqhHqqyUWyVupAKHPlgJU6YHtySmc6VoiHMAAgUFwqEhSjp/AWLKusHDINI21NY62FTaHTYE5gSwNkYJBgN/2IJxf3xDBFMBTAbUUFLd4x5K7PnJty9OPwAB/gFcVo/bkBXLdKVDkyCNgHPmoOGSOPHQlIsYjjUS44iMjUMCCslF7BiliJY6goCl5DAKHjKXAGBmm5ljpZuWbSCp+iFT5Ieax9QZKQJAyxB4lmw2XarF6at0UX20MTjnCxio9JL2oWRFDAJwWMMqqVI2ylmkPDP4BIBA3Ct+12CNQHpsAIK66tKZM4s26baVff8+MyCYAGG4cSUokcytBikCdm5MDvIwRAQAIfkECRkADwAsAAAAADIAMgCDAAAAGCBQaAAAMEiQ4CAA8OgggICAgJDgiND4wMDA+Pj4AAAAAAAAAAAAAAAAAAAABP/wyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPD2DTmw3gmK3cMp1QZzAkgLHhsHjcvXSDwWE6jQIUCgOSBYhSB8LoIHG0urrSajhKRjtVtjTYRsZGFVb126Q7BAIFNgd1eGNsXmYnfX+BAIMJdoaSB4mKXX9QU3hkY2RFaXuWNgQEjgc/bVgKbXopQqSmqEeqrJRbJW6kBIc+WAlTpge3JKZzpWiIcwACBQXCoSFKOn8BYsq6wcMg0jbU1jrYVNodo7oCAkSeA3/YgnF/fEMEUwFMBtRQUt3jHkrzfvbwxdEHIAC/fsvkcRuyYtm5h/kSpBGgC1bDhxAHSpxCsSI0XBiP0Vkbs/CYJYz5BhTRUkcQsIPkHJ5LuRJAy2wwOdiAaCWKkSOQVP14+XGbTCH2tGgZkvHkTCJGigBYKqRpPHRX2gyc8wWMVVwJh1TpGQWBjq/EwgoZC0aM2Z1P+WREIK5qyrMzR8B9OgABXRsGnpJyixdr0R4ysfb9O1XwrrKFdRgVImFhjXME7NxQMoNziAgAIfkECRkADwAsAAAAADIAMgCDAAAAGCBQaAAAMEiQ4CAA8OgggICAgJDgiND4+JiQwMDA+Pj4AAAAAAAAAAAAAAAABP/wyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPD2DTmw3gmL3cMp1QZzAogLHhsHjcvXSDwWE6jQIWCwOSBYhSB8LoQHG0urrSajhKRjtVtjTYRsZGF1b126Q7BAIFNgd1eGNsXmYnfX+BAIMKdoaSB4mKXX9QU3hkY2RFaXuWNgQEjgc/bVgLbXopQqSmqEeqrJRbJW4JCQSHPlgKU6YHtySmcwlxYpRzAAIFBcOhIUo6fwFizLulVMQg1DbW2Drawt0do6QEAgJEngN/BLp9cX98QwRTAUwG1lBS4OY8KMHnZ1+/ZNECBBTY7N63ISuarZvoT0EaAelgRZxIMZnFKRiWM0rDxZEdtjEPS430JnGdvwFFtNQRFGzhuZbsksEsAmBmOZscbFC0EsXIEUiqftRc+UGoSyJGeGoZ0tFSRxv7tEwVUtVezlVN0Mz5AqYrroZDqhCNgkCH2WJohagFI6at05x8OiLgxvWlW5cj7uYcgGAv1qek6v7NyfSC4DiFoxlAzIvtYh0shUh4WGMdATs3lMwQHSICACH5BAkZAA8ALAAAAAAyADIAgwAAABggUGgAADBIkOAgAPDoIICAgICQ4IjQ+PiYkMDAwPj4+AAAAAAAAAAAAAAAAAT/8MlJq7046827/2AojmRpnmiqrqwJtKJhvDAo03UHKAqeb4DFwvfLBIdFzpGYBDifCuEzOXlCpc6m88DlDhaDQXcKA4yt3/BzXD4/04M1l+kyq4X4gSKMN8/pIm9yfT1HWAdZKIJbB4ROeENmiScAYQEFBQKPQoiQQjx+gB5PAQEABKhbUo2cB3FOanUApaepZqt4Xm9xsk8CAgmoX2ALfwACmAGxipVOwAmVYa8ECcFOpaa9dnGlBLC1wQSaVqI6Vq7cAd7NqOHjVnWo61YBMgMG2L/R5SG1oU4GePC4AWvOCn/GAgokWMngwSCMEJGbCE+RDANYzPwaIACVxFryrvjpuJjxwMaOBD7KS1WnT0ZpYKRtmdiSYqWBCxhKHEcJIkU9CjDqdDaJBEBQXBAolbbnEahfUEUa0fdkKbk4JCMd01diqxWraABk9cXVKFR9Ml88geQEgbOoUim8fYeAiym1EPGUenW2qDm4AOoeuPvgkaG9c3mCIBpYmqUsTzAuUFDKbWJNi61cFIdZrsBa0ixXHKVZBmciO0AJCyOaDOkq5C5MlPeqChUJND1EAAAh+QQJZAAPACwAAAAAMgAyAIMAAAAYIFBoAAAwSJDgIADw6CCAgICAkOCI0Pj4mJDAwMD4+PgAAAAAAAAAAAAAAAAE//DJSau9OOvNu/9gKI5kaZ7AqUrGqrYuCQBKGovzYt9gvvMeH7AzK+qKw0zRh0xSiofoYLAYRA/NIeBanFah0ZmWO/MOwNjfapYGLN5TBfW9FLu2WIXiTae5+Tp6BnYoYXqAMzWAgQqDajJTUIt/fZSEMgADAQEFAgKZczp8A4dvNZc4mZudn150o6ULp489AJsBAAS6ZXBVZ4lvjrQfdQCeCbtTkblFeksmxcfJyjMEzbPDIWUzAgnI27uCWWtbkbcGBgMGt3zOQMafMwZ6gsLjMfBF8/ToxTdFugjE03QrUphdqFIJ/OSpiIApAha24bYLWq6GDblBlFgnojWLxb7SzFF2xlO8ZyXq+DKCbgG6QSYbkjOyzE3Ll/BMZiPGpyUcXr4ixcyIaY8sZ9zYTAnAZajOEcCOzop3YGlTp5+gAthHo4bOLUvCYPWUUp8Bkw+2GQOV02lCImbRqm1VcuxbDm4lsN2UaS63odraTiiCAMEBvkwPIGjSNisxwXpnFD6MK/FiO413Wvg6uMxSxJuoVchZ64kbkgOsUSSg7LIFzaN/5ECt2piu1glhayhTOOAOeAR+6k6FsnPxDhEAACH5BAkZAA8ALAAAAAAyADIAgwAAABggUGgAADBIkOAgAPDoIICAgICQ4IjQ+PiYkMDAwPj4+AAAAAAAAAAAAAAAAAT/8MlJq7046827/2AojmRpmgBwro9hsKcLl4CiqLMILAueg7veLxT0DTWpFC9pPFaYy6TzmTpYB7yB9SB1ArZJ7GKQBDdhX2s4W1afT9Ukb2wT96rcWbqmmPdsQXM3XyksezZ+fIE8g1xvOgNkVYmLbgCScJcBAQUCApdiSmwAAgUFAZGPHikDm52fkVFzkqWnqSgAmwEABL2sY1ipvQQpuqodTCmeCb6RqbwJCZ/JuEzLzc4pBNHTTIaXytHEwtueoMc6X6m6BpEGxpdqQ6TdBjY2LknTR/RJ9vf5lBXSo63XtFa6Unka1oUGL3PmrBAQEElAL0fTLBLDNfFTxAMTyCteFEjKV7VkkmIBI6OwG7pVonYBMLDARU0DuQLceQkkZgqaNvNt2qnHHCJWYrIxIplpmoKAX1C1svSvHy6nUA9IDUAVkDkTnpweJdQmyD16AmiETdGnJqt+qWx6NcfTAlq2PHCCMxeX5tx9IsKuXZIq5dsdygSn7Sm4UBIECLQWs4KgC9qvHxLTffA4skyuByoPvPwJxFojSBEW25TtSdiediE7m6islzPRdmE/kR2JdkkCtwdSqIsklkEc9AiM8kJtQjLiDyIAACH+ClBob3RvU2NhcGUAOw==')";

    setTimeout(() => {
        removeFaseView();
    }, 2500);
}

function removeFaseView() {
    let divFaseView = document.getElementById('faseView');
    divFaseView.style.visibility = 'hidden';
    divFaseView.style.opacity = '0';

    setTimeout(() => {
        document.getElementById('faseView').remove();
    }, 500);
}

function gameOver(tipoGameOver) {
    let faseView = document.createElement('div');
    faseView.style.visibility = 'hidden';
    faseView.style.opacity = '0';
    faseView.setAttribute('id', 'faseView');
    faseView.classList.add('faseView');
    faseView.style.zIndex = '99999';
    let divFase = document.createElement('div');
    divFase.classList.add('faseAtual');
    let pFase1 = document.createElement('p');
    pFase1.classList.add('pFase1');
    let pFase2 = document.createElement('p');
    pFase2.classList.add('pFase2')

    if (tipoGameOver == 'morreu') {

        pFase1.innerHTML = 'Morreu';
        pFase2.innerHTML = "Tente de novo <span class='spanBoaSorte'>!!!</span>";
        divFase.style.width = '350px';
        divFase.style.marginLeft = '-175px';
        divFase.appendChild(pFase1);
        divFase.appendChild(pFase2);

    } else if(tipoGameOver == 'tempo')  {

        pFase1.innerHTML = 'Acabou seu tempo';
        pFase2.innerHTML = "Tente de novo <span class='spanBoaSorte'>!!!</span>";
        divFase.style.width = '350px';
        divFase.style.height = '200px';
        divFase.style.marginLeft = '-175px';
        divFase.style.marginTop = '-100px';
        divFase.appendChild(pFase1);
        divFase.appendChild(pFase2);

    } else if (tipoGameOver == 'fimDeJogo') {

        pFase1.innerHTML = "Fim de Jogo";
        divFase.style.width = '350px';
        divFase.style.height = '90px';
        divFase.style.marginLeft = '-175px';
        divFase.style.marginTop = '-45px';
        divFase.appendChild(pFase1);

    }

    faseView.appendChild(divFase);
    document.getElementsByClassName('painelJogo')[0].appendChild(faseView);

    setTimeout(() => {
        let divFaseView = document.getElementById('faseView');
        divFaseView.style.visibility = 'visible';
        divFaseView.style.opacity = '1';
    }, 100);
}
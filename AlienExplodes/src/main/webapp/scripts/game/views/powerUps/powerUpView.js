function geraPowerUpView(tipoPowerUp, divRecebe) {
    let pdus = document.createElement("div");
    pdus.classList.add("powerUp");
    pdus.setAttribute("style", `background-image: url(imagens/sprites/PowerUps/${tipoPowerUp}.gif)`);
    pdus.setAttribute("id", tipoPowerUp);
    powerUps.push(pdus);
    divRecebe.appendChild(pdus);
}
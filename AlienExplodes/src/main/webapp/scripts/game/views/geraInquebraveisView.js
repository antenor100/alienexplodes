function populaTabelaComInquebraveis(idTable) {

    let vetFilhos = document.getElementById(idTable).children;
    let contaMuros = 1;
    
    for(let i=0; i<vetFilhos.length; i++){

        if ( parseInt((vetFilhos[i].getAttribute("id")).slice(1))%2 == 0 ) {
            
            let celulasLinhaI = vetFilhos[i].children;

            for(let j=0; j<celulasLinhaI.length; j++){

                if ( parseInt((celulasLinhaI[j].getAttribute("id")).slice(1))%2 == 0) {
                	
                	let imgBloco = arrayBlocosInquebraveis[Math.floor(Math.random() * arrayBlocosInquebraveis.length)];
             
                    let divAppend = document.createElement("div");
                    divAppend.setAttribute("id","muro" + contaMuros)
                    divAppend.classList.add("bloco");
                    divAppend.style.backgroundImage = `url(imagens/sprites/blocos/blocoInquebravel${imgBloco}.png)`;

                    let filhosTr = document.getElementById(vetFilhos[i].getAttribute("id")).children;

                    filhosTr[j].appendChild(divAppend);
                    contaMuros++;

                }
            }
        }
    }
}
function alteraSpritePraDirecao(idDivDirecao, idpersonagem) {
    
    ControleMovimentoPersonagem.defineUltimoMovimento(idDivDirecao);

    let eventoTecla = document.getElementById('ultimoMovimento').innerHTML;

    let top = document.getElementById(idpersonagem).style.top;
    let left = document.getElementById(idpersonagem).style.left;

    if (eventoTecla == 'L') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/ladoEsquerdoAndando.gif)`);

    }else if (eventoTecla == 'U') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/costaAndando.gif)`);

    }else if (eventoTecla == 'R') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/ladoDireitoAndando.gif)`);

    }else if (eventoTecla == 'D') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/frenteAndando.gif)`);
    } 
}

function alteraSpritePersonagemParado(idpersonagem) {
    let eventoTecla = document.getElementById('ultimoMovimento').innerHTML;
    let top = document.getElementById(idpersonagem).style.top;
    let left = document.getElementById(idpersonagem).style.left;

    if (eventoTecla == 'L') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/ladoEsquerdo.png)`);

    }else if (eventoTecla == 'U') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/costa.png)`);

    }else if (eventoTecla == 'R') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/ladoDireito.png)`);

    }else if (eventoTecla == 'D') {

        document.getElementById(idpersonagem).setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/${idpersonagem}/frente.png)`);
    } 
}

function alteraSpritePersonagemMorto() {

    let top = document.getElementById("personagem").style.top;
    let left = document.getElementById("personagem").style.left;
    
    document.getElementById("personagem").setAttribute("style", `top:${top}; left:${left}; background-image: url(./imagens/sprites/personagem/morto.gif)`);
}
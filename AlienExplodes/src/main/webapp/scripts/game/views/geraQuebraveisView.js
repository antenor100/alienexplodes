function populaTabelaComQuebraveis(idTable) {

    let vetFilhos = document.getElementById(idTable).children;
    let contaMuros = 46;
    let blocosLinhaImpar = [4,6];
    let blocosLinhaPar = [3,5];
    let blocosQuebraveis = [];

    contaMuros = appendBlocoQuebravel(0, 2, vetFilhos, blocosQuebraveis, contaMuros);
    contaMuros = appendBlocoQuebravel(2, 0, vetFilhos, blocosQuebraveis, contaMuros);

    for (let i=0; i<vetFilhos.length; i++) {

        if (parseInt((vetFilhos[i].getAttribute("id")).slice(1))%2 == 1) {

            if (i == 0) {
                for (let j = 3; j < numeroColunas; j++) {
                    blocosQuebraveis[j] = j;
                }
            }
            else if (i == 2) {
                for (let j = 1; j < numeroColunas; j++) {
                    blocosQuebraveis[j] = j;
                }
            }
            else {
                for (let j = 0; j < numeroColunas; j++) {
                    blocosQuebraveis[j] = j;
                }
            }

            blocosQuebraveis.sort((a, b) => 0.5 - Math.random());
            let blocos = sortearQuantBlocos(blocosLinhaImpar);

            for (let j = 1; j <= blocos; j++) {
            	
                contaMuros = appendBlocoQuebravel(i, j, vetFilhos, blocosQuebraveis, contaMuros);

            }
        }
        else {

            if (i < 2) {
                for (let j = 2; j < numeroColunas; j++) {
                    blocosQuebraveis[j] = j;
                }
            }
            else {
                for (let j = 0; j < numeroColunas; j++) {
                    blocosQuebraveis[j] = j;
                }
            }
            
            blocosQuebraveis = blocosQuebraveis.filter((value) => value%2 == 0);
            blocosQuebraveis.sort((a, b) => 0.5 - Math.random());
            let blocos = sortearQuantBlocos(blocosLinhaPar);

            for (let j = 1; j <= blocos; j++) {

                contaMuros = appendBlocoQuebravel(i, j, vetFilhos, blocosQuebraveis, contaMuros);

            }
        }
    }
}

let sortearQuantBlocos = (vetBlocos) => Math.floor(Math.random() * (vetBlocos[1] - vetBlocos[0] + 1)) + vetBlocos[0];

function appendBlocoQuebravel(i, j, vetFilhos, blocosQuebraveis, contaMuros) {

    let imgBloco = arrayBlocosQuebraveis[Math.floor(Math.random() * arrayBlocosQuebraveis.length)];
    let divAppend = document.createElement("div");
    divAppend.setAttribute("id","muro" + contaMuros);
    divAppend.classList.add("blocoQuebravel");
    divAppend.style.backgroundImage = `url(imagens/sprites/blocos/blocoQuebravel${imgBloco}.png)`;
    arrayObjsObstaculosDestrutiveis.push({id:("muro"+contaMuros)});

    let filhosTr = document.getElementById(vetFilhos[i].getAttribute("id")).children;

    if (blocosQuebraveis.length != 0) {
        if (filhosTr[blocosQuebraveis[j]].children.length < 1) {
            filhosTr[blocosQuebraveis[j]].appendChild(divAppend);
        }
    }
    else {
        if (filhosTr[j].children.length < 1) {
            filhosTr[j].appendChild(divAppend);
        }
    }

    contaMuros++;
    return contaMuros;
}
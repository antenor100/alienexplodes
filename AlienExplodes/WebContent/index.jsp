<%@page import="java.util.*"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html lang="pt-br">

    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>AlienExplodes</title>

        <link rel="icon" href="imagens/index/alienexplodes.png" type="image/png">

        <link rel="stylesheet" href="estilos/index/index.css">
        <link rel="stylesheet" href="estilos/index/login.css">
        <link rel="stylesheet"  href="estilos/fontes.css">

        <script src="scripts/index/controllers/cadastroController.js"></script>
        <script src="scripts/index/controllers/indexController.js"></script>
        <script src="scripts/index/controllers/loginController.js"></script>
        <script src="scripts/index/controllers/mudarLoginCadastroController.js"></script>
        <script src="scripts/index/controllers/volumeController.js"></script>
        <script src="scripts/index/controllers/lembrarDadosController.js"></script>
        
        <script src="scripts/index/helpers/sonsIndex.js"></script>
        
        <script src="scripts/index/models/localStorageModel.js"></script>
        <script src="scripts/index/models/doPostServlet.js"></script>

        <script src="scripts/index/views/avisosLoginCadastro.js"></script>
        <script src="scripts/index/views/mostrarSenha.js"></script>
        
    </head>

    <body onresize="fundoResponsivo('divFundo/corpoInteiro')" onload="localGetDadosSalvos(), fundoResponsivo('divFundo/corpoInteiro')">

        <audio src="sons/introPagina01.ogg" id="somFundo" autoplay loop></audio>
        <script>document.getElementById("somFundo").volume = 0.5;</script>

        <div id="divFundo">
            <img id="imgFundo" src="imagens/index/fundo.gif" alt="Cidade destruida" class="imgFundo">    
        </div>

        <div id="corpoInteiro">

            <div id="logo" class="divLogo">
                <img id="imgLogo" src="imagens/index/alienexplodes.png" alt="Logo do jogo" class="imgLogo" onclick="mostraLogin('divLogin', 'logo', 'imgLogo', 'aviso'), buttonPress();">
                <span id="aviso" class="aviso animaPiscando"> Clique na logo para come�ar!</span>
            </div>

            <div class="divVolumeControl" id="divVolumeControl" onmouseenter="mouseEnterVolumeControl()" onmouseleave="mouseLeaveVolumeControl()">
                <img src="imagens/index/volumeHigh.png" alt="Volume" class="imgVolumeControl" id="imgVolumeControl" onclick="mutarSomFundo(), buttonPress();">
                <input type="range" name="inputVolumeControl" class="inputVolumeControl" id="inputVolumeControl" min="0" max="10" value="5" oninput="ajustarVolume(value)">
            </div>

        </div>

        <div id="divLogin" class="divLogin">
            <div class="fecharDiv" onclick="fechaLogin('divLogin', 'logo', 'imgLogo', 'aviso',), buttonPress();">
                <span class="fecharSpan">X</span>
            </div>
            <div class="divEntrar" id="divEntrar">
                <form action="">
                    <table>
                        <tr>
                            <th class="titleLogin">ENTRE</th>
                        </tr>
                        <tr>
                            <th class="subtitleLogin">e salve o mundo !</th>
                        </tr>
                        <tr>
                            <td class="tdNome" id="tdNomeEntrar">
                                <input type="text" id="nomeEntrar" class="inputEntrar inputNomeEntrar" placeholder="Nome" name="usuario" onkeypress="teclar()">
                                <p class="pNomeEntrar" id="pNomeEntrar"><img src="imagens/index/invalid.png" alt="invalid"><span id="spanNomeEntrar"></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdSenha" id="tdSenhaEntrar">
                                <span class="tooltip">
                                    <img src="imagens/index/eye-closed.png" alt="Mostrar Senha" class="eye" id="eyeEntrar" onclick="mostrarSenha('senhaEntrar', 'eyeEntrar', 'mostrarSenhaEntrar'), buttonPress();">
                                    <span class="tooltiptext" id="mostrarSenhaEntrar">Mostrar Senha</span>
                                </span>
                                <input type="password" id="senhaEntrar" class="inputEntrar inputSenhaEntrar" placeholder="Senha" name="senha" onkeypress="teclar()">
                                <p class="pSenhaEntrar" id="pSenhaEntrar"><img src="imagens/index/invalid.png" alt="invalid"><span id="spanSenhaEntrar"></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLembrarDados">
                                <input type="checkbox" id="lembrarDados"><label for="lembrarDados"> Lembrar meus dados</label>
                            </td>
                        </tr>
                        <tr class="trBtnEntrar">
                            <td>
                                <input type="button" value="Entrar" id="btnEntrar" class="btnEntrar" onclick="doPostServletConsulta() , buttonPress();">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdConvidado">
                                <p class="pCursorDefault">OU</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdConvidado">

                                <input type="button" value="Entrar como Convidado" id="btnEntrarConvidado" class="btnEntrar" onclick="buttonPress(), setInterval(() => {window.location.href='game.html'}, 100);">

                            </td>
                        </tr>
                        <tr>
                            <td class="tdCadastrar">

                                <p class="pCursorDefault">Nao tem uma conta? <span class="spanCadastrar" onclick="mostrarCadastro(), buttonPress();">Cadastre-se</span></p>

                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="divCadastrar" id="divCadastrar">
                <form>
                    <table>
                        <tr>
                            <th class="titleLogin">Cadastre-se</th>
                        </tr>
                        <tr>
                            <th class="subtitleLogin">para salvar o mundo !</th>
                        </tr>
                        <tr>
                     		<td>
                     			<p class="pNomeCadastro" style="display:block"><img src="imagens/index/invalid.png" alt="invalid"><span>erro</span></p>
                     		</td>
                        </tr>
                        <tr>
                            <td class="tdCadastro" id="tdNomeCadastro">
                                <input type="text" id="nomeCadastro" class="inputCadastro inputNomeCadastro" placeholder="Nome" name="novoUsuario" onkeypress="teclar()">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdCadastro" id="tdSenhaCadastro">
                                <span class="tooltip">

                                    <img src="imagens/index/eye-closed.png" alt="Mostrar Senha" class="eye" id="eyeCadastro" onclick="mostrarSenha('senhaCadastro','eyeCadastro', 'mostrarSenhaCadastrar'),  buttonPress();">

                                    <span class="tooltiptext" id="mostrarSenhaCadastrar">Mostrar Senha</span>
                                </span>
                                <input type="password" id="senhaCadastro" class="inputCadastro inputSenhaCadastro" placeholder="Senha" name="novaSenha" onkeypress="teclar()">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdCadastro" id="tdConfSenhaCadastro">
                                <span class="tooltip">

                                    <img src="imagens/index/eye-closed.png" alt="Mostrar Senha" class="eye" id="confEyeCadastro" onclick="mostrarSenha('confSenhaCadastro', 'confEyeCadastro', 'mostrarConfSenhaCadastrar'),  buttonPress();">

                                    <span class="tooltiptext" id="mostrarConfSenhaCadastrar">Mostrar Senha</span>
                                </span>
                                <input type="password" id="confSenhaCadastro" class="inputCadastro inputConfSenhaCadastro" placeholder="Confirmar Senha" name="confirmacaoSenha" onkeypress="teclar()">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdCadastro" id="tdEmailCadastro">
                                <input type="text" id="emailCadastro" class="inputCadastro inputEmailCadastro" placeholder="E-mail" name="emailUsuario" onkeypress="teclar()">
                            </td>
                        </tr>
                        <tr class="trBtnCadastrar">
                            <td>
                                <!-- chama classe ajax para obter resposta do servlet -->
                                <input type="button" value="Cadastrar" id="btnCadastrar" class="btnCadastrar" onclick="doPostServletCadastro(), buttonPress()">
                            </td>
                        </tr>
                        <tr class="trBtnCadastrar">
                            <td>

                                <input type="button" value="Voltar" id="btnVoltar" class="btnCadastrar" onclick="mostrarCadastro(), buttonPress();">

                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>

        <script>
            fundoResponsivo('divFundo/corpoInteiro');
            centralizaImagemDiv("logo",500,530);
        </script>

    </body>

</html>